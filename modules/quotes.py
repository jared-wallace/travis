import time
import logging
import inspect

quote_list = [
    "Software is like sex: it's better when it's free. - Linus Torvalds",
    "Man is still the most extraordinary computer of all. - John Fitzgerald Kennedy",
    "Computers are like Old Testament gods; lots of rules and no mercy. - Joseph Campbell",
    "Never trust a computer you can't throw out a window. - Steve Wozniak",
    "Imagine if every Thursday your shoes exploded if you tied them the usual way. This happens to us all the time with computers, and nobody thinks of complaining. - Jef Raskin",
    "I am regularly asked what the average Internet user can do to ensure his security. My first answer is usually 'Nothing; you're screwed'. - Bruce Schneier",
    "It's been my policy to view the Internet not as an 'information highway,' but as an electronic asylum filled with babbling loonies. - Mike Royko",
    "Intelligence is the ability to avoid doing work, yet getting the work done. - Linus Torvalds",
    "The Linux philosophy is 'Laugh in the face of danger'. Oops. Wrong One. 'Do it yourself'. Yes, that's it. - Linus Torvalds",
    "I don't try to be a threat to Microsoft, mainly because I don't really see M$ as competition. Especially not Windows-the goals of Linux and Windows are simply so different. - Linus Torvalds",
    "In God we trust; all others must submit an X.509 certificate. - Charles Forsythe",
    "Failure is not an option -- it comes bundled with Windows. - Anonymous",
    "To err is human... to really foul up requires the root password. - Anonymous",
    "If at first you don't succeed; call it version 1.0 - Anonymous",
    "If Python is executable pseudocode, then perl is executable line noise. - Anonymous",
    "Windows Vista: It's like upgrading from Bill Clinton to George W. Bush. - Anonymous",
    "My software never has bugs. It just develops random features. - Anonymous",
    "Programming is like sex, one mistake and you have to support it for the rest of your life. - Anonymous",
    "Unix is user-friendly. It's just very selective about who its friends are. - Anonymous",
    "Microsoft: \"You've got questions. We've got dancing paperclips.\" - Anonymous",
    "The Internet: where men are men, women are men, and children are FBI agents. - Anonymous",
    "Any fool can use a computer. Many do. - Anonymous",
    "A C program is like a fast dance on a newly waxed dance floor by people carrying razors. - Waldi Ravens",
    "The use of COBOL cripples the mind; its teaching should therefore be regarded as a criminal offense. - E. W. Dijkstra",
    "PHP is a minor evil perpetrated and created by incompetent amateurs, whereas Perl is a great and insidious evil, perpetrated by skilled but perverted professionals. - Jon Ribbens",
    "Perl: The only language that looks the same before and after RSA encryption. - Keith Bostik",
    "Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live. - Martin Golding",
    "To iterate is human, to recurse divine. - L. Peter Deutsch",
    "Don't sweat it -- it's not real life. It's only ones and zeroes.  - Gene Spafford",
    "Real knowledge is to know the extent of one's ignorance. - Confucius",
    "Commenting your code is like cleaning your bathroom: you never want to do it, but it really does create a more pleasant experience for you and your guests. - Ryan Campbell",
    "How rare it is that maintaining someone else's code is akin to entering a beautifully designed building, which you admire as you walk around and plan how to add a wing or do some redecorating. More often, maintaining someone else's code is like being thrown headlong into a big pile of slimy, smelly garbage. - Bill Venners",
    "Program testing can be a very effective way to show the presence of bugs, but is hopelessly inadequate for showing their absence. - E. W. Dijkstra",
    "Manually managing blocks of memory in C is like juggling bars of soap in a prison shower: It's all fun and games until you forget about one of them. - Anonymous",
    "Java is the most distressing thing to hit computing since MS-DOS. - Alan Kay",
    "There's no obfuscated Perl contest because it's pointless. - Jeff Polk",
    "UNIX is simple.  It just takes a genius to understand its simplicity. - Dennis Ritchie",
    "I'm not one of those who think Bill Gates is the devil.  I simply suspect that if Microsoft ever met up with the devil, it wouldn't need an interpreter. - Nicholas Petreley",
    "Build a system that even a fool can use, and only a fool will want to use it. - Anonymous",
    "If you're having trouble sounding condescendent, get a Unix user to show you how. - Scott Adams",
    "Saying your OS is the best in the world 'cause more people use it is like saying McDonalds makes the best food in the world. - Anonymous",
    "Windows 95 /n./ 32 bit extensions and a graphical shell for a 16 bit patch to an 8 bit operating system originally coded for a 4 bit microprocessor, written by a 2 bit company that can't stand 1 bit of competition. - Anonymous",
    "I had a fortune cookie the other day and it said: 'Outlook not so good'. I said: 'Sure, but Microsoft ships it anyway'. - Anonymous",
    "Letting XP run for more than a month is like re-using a condom 50 or 60 times. Theoretically it can work, but is sick and ill advised. - Anonymous",
    "Remember, even paranoids have real enemies.  - Delmore Schwartz",
    "unzip; strip; touch; finger; mount; fsck; more; yes; unmount; sleep: my daily unix command list. - Anonymous",
    "A computer lets you make more mistakes faster than any invention in human history; with the possible exceptions of handguns and tequila. - Mitch Ratcliffe",
    "Relying on the Government to protect your privacy is like asking a peeping Tom to install your window blinds. - John Barlow",
    "There are two major products that came out of Berkeley: LSD and UNIX. We don't believe this to be a coincidence. - Jeremy Anderson",
    "The last good thing written in C++ was the Pachelbel Canon. - Jerry Olsen",
    "I have not failed. I've just found 10,000 ways that won't work.  - Thomas Edison",
    "A good programmer is someone who always looks both ways before crossing a one-way street. - Doug Linder",
    "Macs are for those who don't want to know why their computer works. Linux is for those who want to know why their computer works. DOS is for those who want to know why their computer doesn't work. Windows is for those who don't want to know why their computer doesn't work. - Anonymous",
    "This is LINUX land, in silent nights you can hear the Windows machines rebooting. - Anonymous",
    "Linux is no OS. It's a core dump which boots by accident. - Anonymous",
    "User, n.  The word computer professionals use when they mean \"idiot\". - Dave Barry",
    "2 strings walk into a bar. The first string says to the bartender,\"Bartender, I'll have a beer. u.5n$x5t?*&4ru!2[sACC~ErJ\" The second string says \"Pardon my friend, he isn't NULL terminated.\" - Anonymous",
    "In googlis non est, ergo non est. - Anonymous",
    "Act in haste and repent at leisure; Code too soon and debug forever. - Raymond Kennington",
    "Only two things are infinite: the universe and human stupidity and I'm not sure about the former. - Einstein",
    "It's a hundred and six miles to Chicago, we've got a full tank of gas, half a pack of cigarettes, it's dark, and we're wearing sunglasses. Hit it! - The Blues Brothers",
    "gcc -O4 emails your code to Jeff Dean for a rewrite. - Anonymous",
    "The rate at which Jeff Dean produces code jumped by a factor of 40  in late 2000 when he upgraded his keyboard to USB 2.0 - Anonymous",
    "All pointers point to Jeff Dean. - Anonymous",
    "The speed of light in a vacuum used to be about 35 mph. Then Jeff Dean spent a weekend optimizing physics. - Anonymous",
    "Jeff Dean was born on December 31, 1969 at 11:48 PM. It took him twelve minutes to implement his first time counter. - Anonymous",
    "When Jeff Dean sends an ethernet frame there are no collisions because the competing frames retreat back up into the buffer memory on their source nic. - Anonymous",
    "When Jeff Dean designs software, he first codes the binary and then writes the source as documentation. - Anonymous",
    "Jeff Dean wrote an O(n^2) algorithm once. It was for the Traveling Salesman Problem. - Anonymous",
    "Jeff Dean once implemented a web server in a single printf() call. Other engineers added thousands of lines of explanatory comments but still don't understand exactly how it works. Today that program is the front-end to Google Search. - Anonymous",
    "When your code has undefined behavior, you get a seg fault and corrupted data. When Jeff Dean's code has undefined behavior, a unicorn rides in on a rainbow and gives everybody free ice cream. - Anonymous",
    "When Jeff Dean fires up the profiler, loops unroll themselves in fear. - Anonymous",
    "When Jeff Dean listens to mp3s, he just cats them to /dev/dsp and does the decoding in his head. - Anonymous",
    "Jeff Dean once shifted a bit so hard, it ended up on another computer. - Anonymous",
    "Jeff Dean puts his pants on one leg at a time, but if he had more legs, you would see that his approach is O(log n). - Anonymous",
    "Knuth mailed a copy of TAOCP to Google. Jeff Dean autographed it and mailed it back. - Anonymous",
    "The x86-64 spec includes several undocumented instructions marked 'private use'. They are actually for Jeff Dean's use. - Anonymous",
    "Jeff Dean sorts his phone contacts by their vcard's md5 checksums - Anonymous",
    "Two roads diverged in a wood and Jeff Dean /Took both of them in parallel /And that has made all the difference - Anonymous",
    "Documentation is like sex; when it's good, it's very good, and when it's bad, it's still better than nothing. - Anonymous",
    "If it's your job to eat a frog, it's best to do it first thing in the morning. And If it's your job to eat two frogs, it's best to eat the biggest one first. - Mark Twain",
    ]

logger = logging.getLogger("Travis.news")

def log(message, level):
    '''This allows us to see what function we were in when we got called
    '''
    func = inspect.currentframe().f_back.f_code
    numeric_level = getattr(logging, level.upper(), None)
    logger.log(numeric_level, '%s: %s in %s:%i' % (
        message,
        func.co_name,
        func.co_filename,
        func.co_firstlineno
    ))

def get_quote():
    return quote_list[int(round(time.time() * 1000)) % len(quote_list)]

def search_quotes(terms):
    log('Entered search_quotes', 'debug')
    relevant_results = [(quote) for quote in quote_list for term in terms if term.upper() in quote.upper()]
    relevant_results = list(set(relevant_results))
    if relevant_results:
        # pick random relevant quote
        log('Returning quote', 'debug')
        return relevant_results[int(round(time.time() * 1000)) % len(relevant_results)]
    else:
        log('Returning empty string', 'debug')
        return ""
