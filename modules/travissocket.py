import socket
import os
import sys
import time
import threading
import struct
import travis_parse
import logging
import inspect

class TravisSocket():
    def __init__(self, bot):
        self.logger = logging.getLogger("Travis.socket")
        self.log('Initializing unix domain socket', 'info')
        self.bot = bot
        address = bot.sock_path
        self.log('Socket located at: {0}'.format(address), 'info')

        try:
            os.unlink(address)
        except OSError:
            if os.path.exists(address):
                raise

        # Create socket
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.log('Binding unix domain socket', 'info')
        try:
            self.sock.bind(address)
        except Exception, e:
            self.log('Error binding because {0}'.format(e), 'error')
        os.chmod(address, 0777)
        self.log('Listening on unix domain socket {0}'.format(address), 'info')
        self.sock.listen(1)

        # Spawn UDS thread
        t = threading.Thread(name='SocketThread',
                target=self.loop)
        t.daemon = True
        t.start()

    def log(self, message, level):
        '''This allows us to see what function we were in when we got called
        '''
        func = inspect.currentframe().f_back.f_code
        numeric_level = getattr(logging, level.upper(), None)
        self.logger.log(numeric_level, '%s: %s in %s:%i' % (
            message,
            func.co_name,
            func.co_filename,
            func.co_firstlineno
        ))

    def loop(self):
        while 1:
            self.log('Waiting for UDS connection', 'debug')
            time.sleep(.1)
            connection, client_address = self.sock.accept()
            try:
                self.log('Receiving on UDS from {0}'.format(client_address), 'debug')
                raw_msglen = self.recvall(connection, 4)
                if not raw_msglen:
                    pass
                else:
                    msglen = struct.unpack('>I', raw_msglen)[0]
                    travis_parse.parse(self.recvall(connection, msglen), self.bot)
            except Exception, e:
                self.log('Error reading from UDS in travissocket {0}'.format(e), 'error')
            finally:
                connection.close()

    def recvall(self, sock, n):
        data = ''
        while len(data) < n:
            try:
                packet = sock.recv(n - len(data))
            except Exception, e:
                self.log("Can't get packet because {0}".format(e), 'debug')
            if not packet:
                return None
            #self.log('Got packet {0}'.format(packet), 'debug')
            data += packet
        return data
