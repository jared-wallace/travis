import time
import json
import datetime
import pytz
import threading
import logging
import inspect

import requests
import ConfigParser
import dateutil.parser

from travis_exceptions import InitializationError

class Git():
    def __init__(self, bot):
        self.logger = logging.getLogger("Travis.git")
        self.log('Entered Git init', 'info')
        self.bot = bot
        config = ConfigParser.ConfigParser()
        config.readfp(open(bot.config_file))
        try:
            self.message_color = bot.git_message_color
            self.deploy_color = bot.git_deploy_color
            self.url = config.get('git', 'git_url')
            self.token = config.get('git', 'git_auth')
            self.user = config.get('git', 'git_user')
            self.timezone = config.get('global', 'timezone')
            enabled = config.get('git', 'git_new_push')
            if enabled == 'yes' or enabled == 'Yes' or enabled == 'True' or enabled == 'true':
                self.push_enabled = True
            else:
                self.push_enabled = False
            push_repo_string = config.get('git', 'git_new_push_repositories')
            enabled = config.get('git', 'git_new_issue')
            if enabled == 'yes' or enabled == 'Yes' or enabled == 'True' or enabled == 'true':
                self.issues_enabled = True
            else:
                self.issues_enabled = False
            issue_repo_string = config.get('git', 'git_new_issue_repositories')

        except IOError, e:
            self.log(
                    'Failed to open config file because: {0}'.format(e),
                    'error')
            raise InitializationError
        except ConfigParser.NoSectionError, e:
            self.log(
                'Failed to locate section in config file: {0}'.format(e),
                'error')
            raise InitializationError
        except ConfigParser.NoOptionError, e:
            self.log(
                'Failed to locate option {0} in config file'.format(e),
                'error')
            raise InitializationError
        except Exception, e:
            self.log('Unexpected error {0}'.format(e), 'error')
            raise InitializationError

        try:
            repo_list = issue_repo_string.split()
            # repo list ex. ['TravisMcGee:all', 'ZTools:bugs,assignedme']
            self.issue_repos = {}
            for repo in repo_list:
                name = repo.split(':')[0]
                action_string = repo.split(':')[1]
                action_list = action_string.split(',')
                self.issue_repos[name] = action_list
            # issue_repos ex { 'TravisMcGee': ['all'], 'ZTools': ['bugs', 'assignedme']}
        except Exception, e:
            self.log('Failed to populate issue_repos', 'error')

        try:
            repo_list = push_repo_string.split()
            # repo list ex. ['TravisMcGee:master', 'ZTools:master,development']
            self.push_repos = {}
            for repo in repo_list:
                name = repo.split(':')[0]
                branch_string = repo.split(':')[1]
                branch_list = branch_string.split(',')
                self.push_repos[name] = branch_list
            # push_repos ex { 'TravisMcGee': ['master'], 'ZTools': ['master', 'development']}
        except Exception, e:
            self.log('Failed to populate push_repos', 'error')
        # If we have any alerts enabled, start our monitoring loop
        if self.push_enabled or self.issues_enabled:
            t = threading.Thread(name='GitThread',
                    target=self.monitor,)
            t.daemon = True
            t.start()
        else:
            pass

    def log(self, message, level):
        '''This allows us to see what function we were in when we got called
        '''
        func = inspect.currentframe().f_back.f_code
        numeric_level = getattr(logging, level.upper(), None)
        self.logger.log(numeric_level, '%s: %s in %s:%i' % (
            message,
            func.co_name,
            func.co_filename,
            func.co_firstlineno
        ))

    def monitor(self):
        self.log('Entered monitor', 'info')
        # get etag and initial poll value
        payload = {'access_token': self.token}
        url = [self.url]
        url.append('users/')
        url.append(self.user)
        url.append('/events')
        self.log('Sending intial git event request with {0}'.format(''.join(url)), 'debug')
        r = requests.get(''.join(url), params=payload)
        while r.status_code != requests.codes.ok:
            self.log('Unable to complete initial request to git events', 'error')
            self.log('Response code: {0}'.format(r.status_code), 'error')
            self.log('Retrying in 60 seconds...', 'error')
            time.sleep(60)
            r = requests.get(''.join(url), params=payload)

        poll_interval = int(r.headers['X-Poll-Interval'])
        tag = r.headers['ETag']

        headers = {'If-None-Match': tag}
        time.sleep(poll_interval)
        # enter loop proper
        while 1:
            # poll
            r = requests.get(''.join(url), params=payload, headers=headers)
            if r.status_code == 304:
                # no change
                self.log('Git said no recent changes', 'debug')
                pass
            elif r.status_code == requests.codes.ok:
                self.log('headers are {0}'.format(r.headers), 'debug')
                try:
                    tag = r.headers['ETag']
                    poll_interval = int(r.headers['X-Poll-Interval'])
                except:
                    pass
                headers = {'If-None-Match': tag}
                self.log('Git is announcing changes', 'debug')
                # what changed
                data = json.loads(r.text)
                self.log('Data: {0}'.format(data), 'debug')
                self.log('Latest event: {0}'.format(data[0]), 'debug')
                # Since we could have had multiple events since last poll...
                for event in data:
                    creation = dateutil.parser.parse(event['created_at'])
                    now = datetime.datetime.now()
                    # localize
                    timezone = pytz.timezone(self.timezone)
                    now = timezone.localize(now)
                    # Get time delta
                    delta = now - creation
                    if delta.seconds > 60:
                        # break out to avoid cycling through all 30 events
                        self.log('Current event was too long ago ({0}) seconds ago'.format(delta.seconds), 'debug')
                        self.log('This is probably normal'.format(delta.seconds), 'debug')
                        break
                    else:
                        # handle if necessary
                        if self.push_enabled and event['type'] == 'PushEvent':
                            self.log('Found push event', 'debug')
                            # See if we care or not
                            repo = event['repo']['name'].encode('utf-8').split('/')[1]
                            branch = event['payload']['ref'].encode('utf-8').split('/')[2]
                            if repo not in self.push_repos:
                                self.log("But we don't care about it, because we aren't watching that repo", 'debug')
                                self.log("Repo was {0} and we are watching {1}".format(repo, self.push_repos), 'debug')
                                break
                            found = False
                            for option in self.push_repos[repo]:
                                if option == branch:
                                    found = True
                            if not found:
                                self.log("But we don't care about it, because we aren't watching that branch", 'debug')
                                self.log("Branch was {0} and options were: {1}".format(branch, self.push_repos), 'debug')
                                break
                            message = ['New push to repository *']
                            message.append(repo)
                            message.append('* on branch ')
                            message.append(branch)
                            message.append(':\n')
                            for commit in event['payload']['commits']:
                                message.append('*')
                                message.append(commit['sha'].encode('utf-8')[:7])
                                message.append('* ')
                                message.append(commit['message'].encode('utf-8'))
                                message.append(' - _')
                                message.append(commit['author']['name'].encode('utf-8'))
                                message.append('_\n')
                            self.bot.sendMessage(''.join(message).encode('utf-8'), self.deploy_color)
                        elif self.issues_enabled and event['type'] == 'IssuesEvent' or event['type'] == 'IssueCommentEvent':
                            self.log('Caught event type {0}'.format(event['type']), 'debug')
                            # See if we care
                            repo = event['repo']['name'].split('/')[1].encode('utf-8')
                            if repo not in self.issue_repos:
                                self.log('Event was for repo {0}, ignoring'.format(repo), 'debug')
                                break
                            bug = False
                            try:
                                label = event['payload']['issue']['label']
                                if label is not None and label.encode('utf-8') == 'bug':
                                    bug = True
                            except Exception, e:
                                self.log('Failed to obtain label', 'debug')

                            found = False
                            try:
                                assignee_list = event['payload']['issue']['assignees']
                                if assignee_list is not None:
                                    for person in assignee_list:
                                        if self.user == person['login']:
                                            found = True
                            except Exception, e:
                                self.log('Failed to obtain assignee list', 'debug')

                            if 'all' not in self.issue_repos[repo]:
                                if 'bugs' in self.issue_repos[repo] and bug:
                                    # continue
                                    self.log('Found a bug issue', 'debug')
                                    pass
                                elif 'assignedme' in self.issue_repos[repo] and found:
                                    self.log('Found an issue assigned to me', 'debug')
                                    # continue
                                    pass
                                else:
                                    # We're done here
                                    self.log("Issue didn't match our filter", 'debug')
                                    break
                            else:
                                    self.log("Filter was set to 'all', issue caught", 'debug')

                            try:
                                title = event['payload']['issue']['title'].encode('utf-8')
                                body = event['payload']['issue']['body'].encode('utf-8')
                                # Now let's figure out what kind of issue event
                                # options include: "assigned", "unassigned", "labeled",
                                # "unlabeled", "opened", "edited", "milestoned", "demilestoned",
                                # "closed", or "reopened".
                                action = event['payload']['action'].encode('utf-8')
                                number = str(event['payload']['issue']['number'])
                                issue_url = str(event['payload']['issue']['url']).encode('utf-8')
                            except Exception, e:
                                self.log('Failed to set either title, body, action or issue number because {0}'.format(e), 'error')

                            if event['type'] == 'IssuesEvent':
                                # "[Issue # number](url) `title` *action* by author"
                                message = ['[Issue #']
                                message.append(number)
                                message.append('](')
                                message.append(issue_url)
                                message.append(') *')
                                message.append(title)
                                message.append('* **')
                                message.append(action)
                                if action == 'assigned':
                                    message.append('to ')
                                if action == 'unassigned':
                                    message.append('from ')
                                try:
                                    assignee = event['payload']['issue']['assignee']
                                except:
                                    self.log('failed to obtain assignee', 'error')
                                if assignee is not None and action == 'assigned' or action == 'unassigned':
                                    message.append(assignee['login'].encode('utf-8'))
                                message.append('** ')

                                message.append(' by ')
                                try:
                                    actor = event['actor']['display_login'].encode('utf-8')
                                    message.append(actor)
                                except:
                                    self.log('Failed to get actor', 'error')
                                    message.append('unknown entity')
                                message.append('. \n```\n')
                                message.append(body)
                                message.append('\n```\n')
                                self.bot.sendMessage(''.join(message), self.message_color)
                            elif event['type'] == 'IssueCommentEvent':
                                issue_title = event['payload']['issue']['title'].encode('utf-8')
                                comment = event['payload']['comment']['body'].encode('utf-8')
                                comment_url = event['payload']['comment']['url'].encode('utf-8')
                                author = event['payload']['comment']['user']['login'].encode('utf-8')
                                # "New [comment](comment url) on [issue # 20](url) - title*: _comment_ by author

                                message = ['New [comment](']
                                message.append(comment_url)
                                message.append(') for [issue #')
                                message.append(number)
                                message.append('](')
                                message.append(issue_url)
                                message.append(') - *')
                                message.append(issue_title)
                                message.append('* \n```\n')
                                message.append(comment)
                                message.append('\n```\n by ')
                                message.append(author)
                                self.bot.sendMessage(''.join(message).encode('utf-8'), self.message_color)
            else:
                self.log('Got bad response from Git', 'error')
                self.log('Code {0}'.format(r.status_code), 'error')
                pass

            time.sleep(poll_interval)


