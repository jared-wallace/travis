import json
import logging
import inspect

logger = logging.getLogger("Travis.queues")

class queue():
    def __init__(self):
        log('Entered queue init', 'info')
        # load data
        try:
            fp = open('modules/queues.json', 'rb')
            self.data = json.loads(fp.read())
            fp.close()
            log('queue data is: {0}'.format(self.data), 'debug')
        except Exception, e:
            log('Failed to load queue data because {0}'.format(e), 'error')

    def find_queue(self, keys):
        """takes a list of keywords to search queue list for
        """
        keywords = [key.lower() for key in keys]
        log('Entered find_queue', 'info')
        log('Keywords are {0}'.format(keywords), 'debug')
        result = []
        for keyword in keywords:
            if keyword in self.data:
                result.append(self.data[keyword])
                log('Found queues {0}'.format(self.data[keyword]), 'debug')
        if result:
            queues = set(result[0]).intersection(*result)
            log('Returning {0}'.format(queues), 'debug')
            return queues
        else:
            log('Found no matching queues', 'debug')
            return None

    def get_queue(self, target):
        log('Entered get_queue', 'info')
        log('Queue target is {0}'.format(target), 'debug')
        keywords = [kw.encode('utf-8') for kw in self.data for q in self.data[kw] if target in q]
        log('Keywords are {0}'.format(keywords), 'debug')
        queue = set([q for kw in self.data for q in self.data[kw] if target in q]).pop()
        return "*" + queue + "*: _" + str(keywords).strip('[]') + "_"

    def get_queues(self):
        log('Entered get_queues', 'info')
        result = []
        for keyword in self.data:
            result.append(self.data[keyword])
        if result:
            queues = set(result[0]).union(*result)
            log('Returned full queue list {0}'.format(queues), 'debug')
            return queues
        else:
            log('Queue list is empty', 'debug')
            return None

    def add_queue(self, keys, queue):
        """Accept keywords and queue description
        """
        log('Entered add_queue', 'info')
        log('Keywords: {0}'.format(keys), 'debug')
        log('Queue description: {0}'.format(queue), 'debug')
        # Are we updating an existing queue or adding a new one?
        exists = list(set([q for kw in self.data for q in self.data[kw] if queue in q]))
        if exists:
            return self.add_keywords(keys, queue)

        keywords = [key.lower() for key in keys]
        for key in keywords:
            # if key exists, append the description
            # if not, default is empty list, and we append to that
            self.data.setdefault(key, []).append(queue)
            # just in case we added a dupe
            list(set(self.data[key]))
            log('Keyword {0} now has {1}'.format(key, self.data[key]), 'debug')
        return "Added queue *{0}* with keywords _{1}_".format(queue, str(keys).strip('[]'))

    def remove_queue(self, keys, target):
        """Remove queue from all keywords
        """
        log('Entered remove_queue', 'info')
        # Are we removing keywords from an existing queue?
        if keys is not None:
            return self.remove_keywords(keys, target)
        # for each kw, check to see if there's a queue that matches out target
        # and if so, remove it.
        removed = [self.data[kw].remove(q) or q for kw in self.data for q in self.data[kw] if target in q]
        if len(removed) > 0:
            return "Removed queue *{0}*".format(removed[0])
        else:
            return "Could not remove specified queue"

    def add_keywords(self, keywords, target):
        log('Entered add_keywords', 'info')
        try:
            # Translate queue short form into an actual queue
            queue = list(set([q for kw in self.data for q in self.data[kw] if target in q]))[0]
            # Add queue to existing kw or create new kw and then add queue to it
            for word in keywords:
                self.data.setdefault(word, []).append(queue)
            msg = 'Successfully added keywords _{0}_ to queue *{1}*'.format(str(keywords).strip('[]'), queue)
            log('Successfully added keywords {0} to queue {1}'.format(str(keywords).strip('[]'), queue), 'debug')
        except Exception, e:
            msg = 'Failed to complete operation because {0}'.format(e)
            log('Failed to complete operation because {0}'.format(e), 'error')
        return msg

    def remove_keywords(self, keywords, target):
        log('Entered remove_keywords', 'info')
        log('Keywords: {0}'.format(keywords), 'debug')
        log('Queue short is {0}'.format(target), 'debug')
        try:
            # Translate queue short form into an actual queue
            queue = list(set([q for kw in self.data for q in self.data[kw] if target in q]))[0]
            log('Queue long is {0}'.format(queue), 'debug')
            # Remove queue from keywords specified
            removed = [self.data[key].remove(q) or key for key in keywords for q in self.data[key] if target in q]
            log('Removed is {0}'.format(removed), 'debug')
            # Check for empty keywords, and delete if needed
            for key in removed:
                if not self.data[key]:
                    self.data.pop(key)
                    log('Popped key {0}'.format(key), 'debug')
            msg = 'Successfully removed keywords _{0}_ from queue *{1}*'.format(str(keywords).strip('[]'), queue)
            log('Successfully removed keywords {0} from queue {1}'.format(str(keywords).strip('[]'), queue), 'debug')
        except Exception, e:
            msg = 'Failed to complete operation because {0}'.format(e)
            log('Failed to complete operation because {0}'.format(e), 'error')
        return msg

    def save_queues(self):
        try:
            fp = open('modules/queues.json', 'wb')
            fp.write(json.dumps(self.data))
            fp.close()
            log('queue data saved', 'info')
        except Exception, e:
            log('Error saving queue data because {0}'.format(e), 'error')


def log(message, level):
    '''This allows us to see what function we were in when we got called
    '''
    func = inspect.currentframe().f_back.f_code
    numeric_level = getattr(logging, level.upper(), None)
    logger.log(numeric_level, '%s: %s in %s:%i' % (
        message,
        func.co_name,
        func.co_filename,
        func.co_firstlineno
    ))
