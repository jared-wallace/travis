import json
import feedparser
import re
import weather
import search
import requests
import datetime
import logging
import inspect
import threading
import time
import pytz
import random

from dateutil import parser

from insults import get_insult
from quotes import get_quote
from travis_exceptions import ParseError

logger = logging.getLogger("Travis.travis_parse")

def parse(data, bot):
    log('Reached parse', 'info')
    data = json.loads(data)
    # Did we get valid travis json?
    if 'type' not in data:
        raise ParseError("Parse can't find type parameter")
    event_type = data['type'].encode('utf-8')
    name = bot.name

    try:
        if event_type == 'gitMessage':
            gitMessage(data, bot)
            return 200
        elif event_type == 'gitDeployment':
            gitDeployment(data, bot)
            return 200
        elif event_type == 'message-created':
            log('Parsing {0}'.format(data), 'debug')
            message = data['content']
            # We get empty messages on occasion
            if message:
                result = analyze(bot, data)
                if result['author'] == bot.name:
                    # Ignore our own messages
                    return 200
                # Check for shortcut
                if result['text'][:1] == '!':
                    # Found shortcut
                    log('Found shortcut', 'info')
                    result['keywords'] = result['text'][1:].split()
                    log('Setting keywords to: {0}'.format(result['keywords']), 'debug')
                    result['text'] = result['text'][1:]
                elif ('entities' not in result or bot.name not in result['entities']) and ('keywords' not in result or bot.name.lower() not in result['keywords']):
                        # add keywords and we're done
                        if result['mispelled']:
                            handle_mispelling(bot, result)
                        log("We're not mentioned (or possibly we just didn't get results back fast enough), try to add keywords", 'debug')
                        add_keywords(bot, result)
                        return
                log('Message mentioned us or we got a shortcut', 'debug')
                if 'keywords' not in result:
                    log('Entities was present, but not keywords...noping out', 'error')
                    return
                keywords = [kw.lower() for kw in result['keywords']]
                if 'add' in keywords and 'word' in keywords:
                    add_word(bot, result)
                elif 'start' in keywords and 'type' in keywords and 'test' in keywords:
                    type_test(bot)
                elif 'remove' in keywords and 'word' in keywords:
                    remove_word(bot, result)
                elif 'add' in keywords and 'keyword' in keywords and 'queue' not in keywords and 'queues' not in keywords:
                    add_keyword(bot, result)
                elif 'show' in keywords and 'keywords' in keywords:
                    bot.sendMessage('Current keywords are: {0}'.format(bot.news_object.keywords), bot.std_color)
                elif 'mute' in keywords and 'alerts' in keywords:
                    bot.weather_object.mute = True
                    bot.sendMessage('Weather alerts have been muted. Use "!unmute alerts" to unmute them', bot.std_color)
                elif 'unmute' in keywords and 'alerts' in keywords:
                    bot.weather_object.mute = False
                    bot.sendMessage('Weather alerts have been unmuted.', bot.std_color)
                elif 'queue' in keywords or 'queues' in keywords:
                    handle_queue(bot, result)
                elif 'show' in keywords and 'mute' in keywords and 'status' in keywords:
                    if bot.weather_object.mute:
                        bot.sendMessage('Weather alerts are muted.', bot.std_color)
                    else:
                        bot.sendMessage('Weather alerts are unmuted.', bot.std_color)
                elif 'change' in keywords and 'name' in keywords:
                    change_name(bot, result)
                elif 'show' in keywords and 'name' in keywords:
                    bot.sendMessage('My name is {0}'.format(bot.name), bot.std_color)
                    time.sleep(1)
                    bot.sendMessage('*NOW SAY MY NAME!!!*', '#FF0000')
                elif 'funny quote' in keywords or 'quote' in keywords:
                    bot.sendMessage(get_quote(), bot.std_color)
                elif 'seen' in keywords:
                    t = threading.Thread(name='LastSeenThread',
                            target=last_seen,
                            args=(bot, result))
                    t.daemon = True
                    t.start()
                elif 'weather' in result['keywords']:
                    log('Got weather request', 'debug')
                    get_weather(bot, result, 'conditions')
                elif 'forecast' in keywords or 'weather forecast' in keywords:
                    log('Got forecast request', 'debug')
                    get_weather(bot, result, 'forecast')
                elif 'xkcd' in keywords or 'latest xkcd' in keywords:
                    log('Got XKCD request', 'debug')
                    get_xkcd(bot)
                elif 'google' in keywords or ('entities' in result and 'Google' in result['entities']):
                    log('Got Google request', 'debug')
                    get_search_results(bot, result)
                elif 'bing' in keywords or ('entities' in result and 'Bing' in result['entities']):
                    bot.sendMessage(get_insult('bing_insults'), bot.std_color)
                elif 'rr' in keywords:
                    bot.sendMessage('{0}'.format(random.randint(0,bot.size_of_die)), bot.std_color)
                elif 'help' in keywords or 'right questions' in keywords:
                    log('Got help request', 'debug')
                    help(bot)
                else:
                    # Check if we're being insulted
                    log('Are we being insulted?', 'debug')
                    author = result['author'].split()[0]
                    if 'sentiment' in result and result['sentiment'] is not None and result['sentiment'] < bot.insult_threshold:
                            log('Why yes we are, responding in kind', 'debug')
                            msg = author + ', ' + get_insult()
                            bot.sendMessage(msg, bot.std_color)
                    else:
                        # some random comment about us or a bad command
                        log("Apparently not, let's see what we got here", 'debug')
                        if result['mispelled']:
                            action = handle_mispelling(bot, result)
                            if action:
                                return
                        text = result['text']
                        author = author.capitalize()
                        punct = text[len(text)-1:]
                        if punct == '?':
                            words = [w[0] for w in bot.tokenizer(text.lower())]
                            try:
                                # A shortcut may not have the bot name, yet
                                # could end up here
                                words.remove(bot.name.lower())
                            except:
                                pass
                            result['keywords'] = words
                            ret = get_search_results(bot, result)
                            if ret == 404:
                                msg = ["I'm sorry, "]
                                msg.append(author)
                                msg.append(", my responses are limited. You'll have to ask the right questions.")
                            else:
                                bot.sendMessage('_Results courtesy of Google_', bot.std_color)
                                return 200
                        elif punct == '.':
                            msg = ["I'm sorry "]
                            msg.append(author)
                            msg.append(", I'm afraid I can't do that.")
                        elif punct == '!':
                            msg = [author]
                            msg.append(", shouting will get you nowhere.")
                        else:
                            msg = ["Let's eat Grandma."]
                            msg.append("\nLet's eat*,* Grandma.")
                            msg.append("\n_Correct punctuation can save lives, ")
                            msg.append(author)
                            msg.append("._")
                        bot.sendMessage(''.join(msg), bot.std_color)
            return 200
        elif event_type is not None:
            #log('Got type equal to: {0}'.format(event_type), 'debug')
            return 200
        else:
            message = 'Sorry, I did not understand that'
            bot.sendMessage(message, bot.err_color)
            return 400
    except Exception, e:
        raise ParseError("Parse failed because: {0}".format(e))

def type_test(bot):
    t = threading.Thread(name='TypeThread',
            target=type_execute,
            args=(bot,))
    t.daemon = True
    t.start()

def type_execute(bot):
    while True:
        bot.sendMessage(get_quote())
        time.sleep(1)

def handle_queue(bot, result):
    """show all queues: "!show queues" or "Jarvis, show me all your queues"
       find queue: "!what queue XXX" or "Jarvis, what queue handles XXX"
       add queue: "!add queue 'description' name
    """
    log('Entered handle_queue', 'info')
    tokenizer = bot.tokenizer
    keywords = [key.upper() for key in result['keywords']]
    single = False
    multiple = False
    if bot.name.upper() in keywords:
        keywords.remove(bot.name.upper())
    if 'QUEUE' in keywords:
        single = True
        keywords.remove('QUEUE')
    if 'QUEUES' in keywords:
        multiple = True
        keywords.remove('QUEUES')

    text = result['text'].upper()

    message = ["```\n"]
    if ('SHOW' in keywords or 'SHOW' in text) and multiple:
        queue_set = bot.queues.get_queues()
    elif ('SHOW' in keywords or 'SHOW' in text) and single:
        res = re.search(r'\'(.+?)\'', result['text'])
        if res is not None:
            queue = res.groups()[0]
        else:
            # invalid syntax
            bot.sendMessage("Sorry, that syntax was invalid. Try: \"{0}, show me queue 'queue name'\"".format(bot.name), bot.err_color)
            return
        bot.sendMessage(bot.queues.get_queue(queue), bot.std_color)
        return
    elif 'WHAT' in keywords or ('WHAT' in text and 'HANDLE' in text):
        words = [w[0] for w in tokenizer(text)]
        # snag everything after 'handles' or 'handle'
        try:
            keywords = words[words.index('HANDLE') + 1:]
        except:
            keywords = words[words.index('HANDLES') + 1:]
        queue_set = bot.queues.find_queue(keywords)
    elif 'ADD' in keywords and 'NEW' in keywords:
        text = result['text']
        res = re.findall(r'\'(.+?)\'', text)
        num_results = len(res)
        if num_results == 2:
            queue = res[0]
            keywords = res[1].split()
        else:
            log('Invalid syntax', 'debug')
            bot.sendMessage("Command must be \"!add new queue 'queue name' with keywords 'kw...'\"", bot.err_color)
            return
        bot.sendMessage(bot.queues.add_queue(keywords, queue), bot.std_color)
        return
    elif 'ADD' in keywords:
        text = result['text']
        res = re.findall(r'\'(.+?)\'', text)
        num_results = len(res)
        if num_results == 2:
            queue = res[1]
            keywords = res[0].split()
        else:
            log('Invalid syntax', 'debug')
            bot.sendMessage("Command must be \"!add keywords 'kw' to queue 'queue name'\"", bot.err_color)
            return
        bot.sendMessage(bot.queues.add_queue(keywords, queue), bot.std_color)
        return
    elif 'REMOVE' in keywords:
        text = result['text']
        res = re.findall(r'\'(.+?)\'', text)
        num_results = len(res)
        if num_results == 1:
            queue = res[0]
            keywords = None
        elif num_results == 2:
            keywords = res[0].split()
            queue = res[1]
        else:
            log('Invalid syntax', 'debug')
            bot.sendMessage("Command must be \"!remove queue 'queue name'\" or \"!remove keywords 'kw' from queue 'queue name'\"", bot.err_color)
            return
        bot.sendMessage(bot.queues.remove_queue(keywords, queue), bot.std_color)
        return
    else:
        log('Hit queue processing, but nothing we expected. May be red herring', 'info')
        return
    for queue in queue_set:
        message.append(queue)
        message.append('\n')
    message.append("```")
    bot.sendMessage(''.join(message), bot.std_color)


def last_seen(bot, result):
    found = False
    has_next_page = False
    cursor = ""
    keywords = result['keywords']
    keywords.remove('seen')
    target = keywords[0]

    # First, let's get a list of Space members
    body = ["{ space(id:"]
    body.append("\"{0}\")".format(bot.spaceid))
    body.append("{ members (first: 200) { items { displayName }")
    body.append(" pageInfo { startCursor endCursor hasNextPage hasPreviousPage } } } }")
    data = make_graphql_call(bot, ''.join(body))
    page = data['data']['space']['members']['pageInfo']

    member_list = []
    for member in data['data']['space']['members']['items']:
        member_list = member_list + member['displayName'].upper().split()
    while page['hasNextPage']:
        body = ["{ space(id:"]
        body.append("\"{0}\")".format(bot.spaceid))
        body.append("{ members (first: 200, after: ")
        body.append("\"{0}\"".format(page['endCursor']))
        body.append(") { items { displayName }")
        body.append(" pageInfo { startCursor endCursor hasNextPage hasPreviousPage } } } }")
        data = make_graphql_call(bot, ''.join(body))
        for member in data['data']['space']['members']['items']:
            member_list = member_list + member['displayName'].upper().split()
        page = data['data']['space']['members']['pageInfo']


    if target.upper() not in member_list:
        author = result['author']
        message = author + ', ' + get_insult("spelling_insults")
        bot.sendMessage(message, bot.err_color)
        bot.sendMessage('{0} is not in here'.format(target), bot.err_color)
        return

    # Let's get last 200 messages (most we can grab)
    body = ["{ conversation"]
    body.append("(id: \"{0}\")".format(bot.spaceid))
    body.append(" { messages (first: 200) { pageInfo { startCursor")
    body.append(" endCursor hasPreviousPage hasNextPage } items { content created")
    body.append(" createdBy { displayName } } } } }")
    data = make_graphql_call(bot, ''.join(body))
    if data is None:
        return 500

    message_list = data['data']['conversation']['messages']['items']
    page = data['data']['conversation']['messages']['pageInfo']
    has_next_page = page['hasNextPage']
    cursor = page['endCursor']
    msg = search_messages(bot, target, message_list)
    if msg:
        found = True
    if not found:
        # Keep looking
        while not found and has_next_page:
            body = ["{ conversation"]
            body.append("(id: \"{0}\")".format(bot.spaceid))
            body.append(" { messages (first: 200, after:")
            body.append("\"{0}\"".format(cursor))
            body.append(") { pageInfo { startCursor")
            body.append(" endCursor hasPreviousPage hasNextPage } items { content created")
            body.append(" createdBy { displayName } } } } }")
            data = make_graphql_call(bot, ''.join(body))
            if data is None:
                return 500
            message_list = data['data']['conversation']['messages']['items']
            page = data['data']['conversation']['messages']['pageInfo']
            has_next_page = page['hasNextPage']
            cursor = page['endCursor']
            msg = search_messages(bot, target, message_list)
            if msg:
                found = True
    if msg:
        bot.sendMessage(''.join(msg), bot.std_color)
    else:
        bot.sendMessage("You sure you typed that name right? I ain't finding any trace of them", bot.err_color)

def make_graphql_call(bot, body):
    if bot.token_expires < (time.time() - 2):
        bot.getAuthorized()
    url = bot.api_url + '/graphql'
    headers = {
        "Content-Type": "application/graphql",
        "x-graphql-view": "PUBLIC",
        "jwt": bot.token}
    try:
        resp = requests.post(url=url, headers=headers, data=body)
    except Exception, e:
        log('Failed to make request because {0}'.format(e), 'debug')
    if resp.status_code == 200:
        data = resp.json()
        log("Got GraphQL response {0}".format(data), 'debug')
        return data
    else:
        log('Failed graphql call', 'error')
        log('Status code was {0}'.format(resp.status_code), 'error')
        log('Response text was {0}'.format(resp.text), 'error')
        return None

def search_messages(bot, target, message_list):
    log('Entering search_messages', 'info')
    for message in message_list:
        author = message['createdBy']['displayName']
        if target.upper() in author.upper():
            timezone = pytz.timezone(bot.timezone)
            date = parser.parse(message['created'])
            date = date.astimezone(timezone)
            text = message['content']
            msg = [author]
            msg.append(date.strftime(" was last seen on %B %-d at %-I:%M %p - _"))
            msg.append(text)
            msg.append("_")
            log('Returning {0}'.format(msg), 'debug')
            return msg
    log('Found nothing', 'debug')
    return None



def change_name(bot, msg):
    log('Entered name change', 'info')
    keywords = msg['keywords']
    if 'entities' in msg:
        entities = msg['entities']
    else:
        entities = []
    current_name = bot.name

    keywords.remove('change')
    keywords.remove('name')

    if keywords:
        name = ''.join(keywords)
        bot.name = name.capitalize()
        message = 'My name has been changed to {0}'.format(bot.name)
    elif entities:
        if current_name in entities:
            entities.remove(current_name)
        if entities:
            name = ''.join(entities)
            bot.name = name.capitalize()
            message = 'My name has been changed to {0}'.format(bot.name)
        else:
            # no valid name
            message = 'My name has not been changed. You did not supply a valid name.'
    bot.sendMessage(message, bot.std_color)

def add_word(bot, msg):
    # Remove 'add' and 'word'
    msg['keywords'].remove('add')
    msg['keywords'].remove('word')
    if msg['keywords']:
        try:
            for word in msg['keywords']:
                bot.d.add(word)
                bot.sendMessage('Added {0} to dictionary'.format(word), bot.std_color)
        except Exception, e:
            message = ["Couldn't add {0} to the dictionary because {1}.".format(word, e)]
            message.append("Try using the shortcut, like '!add word XXX'")
            message.append(" where XXX is the word you want to add.")
            bot.sendMessage(''.join(message), bot.err_color)

def remove_word(bot, msg):
    color = '#FF0000'
    # Remove 'remove' and 'word'
    msg['keywords'].remove('remove')
    msg['keywords'].remove('word')
    if msg['keywords']:
        try:
            for word in msg['keywords']:
                bot.d.remove(word)
            bot.sendMessage('Removed {0} from dictionary'.format(word), bot.std_color)
        except Exception,e:
            message = ["Couldn't remove {0} from the dictionary because {1}.".format(word, e)]
            message.append("Try using the shortcut, like '!remove word XXX'")
            message.append(" where XXX is the word you want to remove.")
            bot.sendMessage(''.join(message), bot.err_color)

def add_keyword(bot, msg):
    """Add a keyword both to the rolling keywords for immediate search
    and to the config keywords for monitoring new stories.
    """
    color = '#016F4A'
    li = []
    for kw in msg['keywords']:
        li = li + kw.split()
    # remove add and keyword
    li.remove('add')
    li.remove('keyword')

    if not li:
        # no keyword provided
        return
    keyword = ' '.join(li)
    # Need to add 'kw_quantity' entries to make it past the filter
    for i in range(0,bot.news_object.kw_quantity):
        now = datetime.datetime.now()
        bot.lock.acquire()
        bot.rolling_keywords[now] = ' ' + keyword + ' '
        bot.lock.release()
    bot.news_object.keywords.append(keyword)
    bot.sendMessage("Added '{0}' to rss search keywords".format(keyword), bot.std_color)


def handle_mispelling(bot, msg):
    # Allow one mispelled word"
    if msg['mispelled'] < 2:
        return False
    author = msg['author'].split()[0]
    # get insult
    message = author + ', ' + get_insult("spelling_insults")
    bot.sendMessage(message, bot.err_color)

    message = "Pretty sure you meant: \"{0}\"".format(msg['corrected_text'])
    bot.sendMessage(message, bot.err_color)
    return True

def get_search_results(bot, result):
    # First, let's extract any quoted strings
    res = re.search(r'\'(.+?)\'', result['text'])
    if res is not None:
        search_terms = res.groups()[0].split()
    else:
        # No quoted string, just grab keywords/entities
        search_terms = []
        if 'keywords' in result and result['keywords']:
            search_terms = search_terms + result['keywords']
            try:
                search_terms.remove(bot.name)
                search_terms.remove('google')
            except:
                pass
        if 'entities' in result and result['entities']:
            search_terms = search_terms + result['entities']
            try:
                search_terms.remove(bot.name)
                search_terms.remove('google')
            except:
                pass

    if not search_terms:
        return 404
    term = '+'.join(search_terms)
    log('Searching google for {0}'.format(term), 'debug')
    s = search.search(bot.config_file)
    results = s.search_google(term)
    for item in results['list']:
        bot.sendMessage(item, bot.std_color)
    log('Search returned: {0}'.format(results), 'debug')
    return 200


def add_keywords(bot, msg):
    new_keywords = []
    if 'keywords' in msg:
        for kw in msg['keywords']:
            now = datetime.datetime.now()
            bot.lock.acquire()
            bot.rolling_keywords[now] = ' ' + kw + ' '
            bot.lock.release()
            new_keywords.append(kw)
            log(
                'Added kw {0} to rolling_keywords'.format(bot.rolling_keywords[now]),
                'debug')
    if 'entities' in msg:
        for kw in msg['entities']:
            now = datetime.datetime.now()
            bot.lock.acquire()
            bot.rolling_keywords[now] = ' ' + kw + ' '
            bot.lock.release()
            log(
                'Added entity kw {0} to rolling_keywords'.format(bot.rolling_keywords[now]),
                'debug')
    log('new keywords list has {0}'.format(new_keywords), 'debug')

def gitMessage(data, bot):
    '''
    expects the following json:
    data {
        'commit_hash': XXX,
        'author': XXX,
        'commit_msg': XXX
    }
    '''
    try:
        message = '*New commit {0} by {1}* _{2}_'.format(
            data['commit_hash'],
            data['author'],
            data['commit_msg'])
    except Exception, e:
        log('Error: {0}'.format(e), 'debug')
        return
    bot.sendMessage(message, bot.git_message_color)


def gitDeployment(data, bot):
        try:
            log('caught gitDeployment', 'debug')
            message = ['*Deploy*: ' + data['branch']]
            message.append('(' + data['to_commit'])
            message.append(') copied to ' + data['deploy_dir'])
            log(''.join(message), 'debug')
        except Exception, e:
            message = 'Oops, something went wrong'
            log('Error: {0}'.format(e), 'debug')
        bot.sendMessage(message, bot.git_deploy_color)


def get_xkcd(bot):
    try:
        log('Entered xkcd', 'debug')
        rss = feedparser.parse('http://xkcd.com/rss.xml')
        url = rss.entries[0]['summary_detail'][
                'value'].split(
                    'src=')[1].split('title')[0][1:-2]
        bot.sendMessage(url, bot.xkcd_color)
    except Exception, e:
        log('XKCD failed with {0}'.format(e), 'error')


def analyze(bot, data):
    log('Entered analyze', 'info')
    botname = bot.name
    # Get msg id
    msg_id = data['messageId']
    time.sleep(bot.response_lag)
    # Have to escape the bracket literal
    #body = "{{ message (id: \"{0}\") {{createdBy {{ displayName id}} created content contentType annotations }}}}".format(msg_id)
    body = ["{ message (id: \""]
    body.append("{0}\")".format(msg_id))
    body.append("{ createdBy { displayName id }")
    body.append(" created content contentType annotations } }")
    data = make_graphql_call(bot, ''.join(body))
    if data is not None:
        # Snarf data
        #
        # Structure:
        # text: the actual message text
        # author: person or app who wrote it
        # author_id: id of the author
        # date: datetime message was created
        # keywords: any keywords identified
        # entities: any entities identified
        # sentiment: sentiment score (-1 < x < 1)
        # dates: dates, if any, identified in message
        # mispelled: list of words that are mispelled
        msg = {}
        response = data['data']['message']
        msg['text'] = response['content'].encode('utf-8')
        # Remove any funky file attachements
        match = re.search(r'(<\$)', msg['text'])
        if match:
            msg['text'] = msg['text'][:match.start()]
        msg['corrected_text'] = msg['text']
        msg['mispelled'] = 0
        # tokenize it
        d = bot.d
        tokenizer = bot.tokenizer
        tokens = [w for w in tokenizer(msg['corrected_text'])]
        # check each word
        for token_tuple in tokens:
            if botname == token_tuple[0]:
                continue
            if not d.check(token_tuple[0]):
                bad_word = token_tuple[0]
                sentence = msg['corrected_text']
                msg['mispelled'] = msg['mispelled'] + 1
                suggestion_list = d.suggest(bad_word)
                if suggestion_list:
                    good_word = '*' + d.suggest(bad_word)[0] + '*'
                else:
                    good_word = "*???*"
                sentence = sentence.replace(bad_word, good_word)
                msg['corrected_text'] = sentence
        msg['date'] = parser.parse(response['created'])
        if response['createdBy']['id'].encode('utf-8') == bot.appid:
            msg['author'] = botname
        else:
            msg['author'] = response['createdBy']['displayName'].encode('utf-8')
        msg['author_id'] = response['createdBy']['id'].encode('utf-8')
        # each annotation is a json string
        annotations = [json.loads(x) for x in response['annotations']]
        for note in annotations:
            if note['type'] == 'message-nlp-keywords':
                msg['keywords'] = []
                for keyword_struct in note['keywords']:
                    msg['keywords'].append(keyword_struct['text'])
            elif note['type'] == 'message-nlp-docSentiment':
                typeSentiment = note['docSentiment']['type']
                if typeSentiment != 'neutral':
                    msg['sentiment'] = note['docSentiment']['score']
                else:
                    msg['sentiment'] = None
            elif note['type'] == 'message-nlp-entities':
                msg['entities'] = []
                for entities_struct in note['entities']:
                    msg['entities'].append(entities_struct['text'].capitalize())
            elif note['type'] == 'message-nlp-dates':
                msg['dates'] = []
                for date_struct in note['dates']:
                    msg['dates'].append(date_struct['date'])
            else:
                pass
        log('Returning analyzation result {0}'.format(msg), 'debug')
        return msg
    else:
        log('Failed to analyze, returning request for help', 'error')
        log('Status code was {0}'.format(resp.status_code), 'error')
        log('Response text was {0}'.format(resp.text), 'error')
        msg = {}
        msg['text'] = '!help'
        msg['author'] = 'Anon'




def get_weather(bot, msg, service):
    if 'keywords' in msg:
        try:
            msg['keywords'].remove(bot.name)
        except:
            pass
    else:
        msg['keywords'] = []
    message = ' '.join(msg['keywords'])
    try:
        log('Captured weather question', 'debug')
        postal_code = re.search(r'.*(\d{5}(\-\d{4})?)', message)
        city_match = re.search(
            r'(([A-Z][a-z.]+\ [A-Z][a-z]+)|([A-Z][a-z]+))',
            ' '.join(message.split()))
        state_match = re.search(r'([A-Z][A-Z])', ' '.join(message.split()))
        if postal_code is not None:
            try:
                zipcode = postal_code.groups()[0]
                weather.get_weather_conditions(bot, zipcode, '', '', service)
            except Exception, e:
                msg = ['Found postal code in message, but failed to get']
                msg.append('conditions because of {0}'.format(e))
                log(''.join(msg), 'error')
        elif city_match is not None and state_match is not None:
            city = ''
            state = state_match.groups()[0]
            try:
                for match in city_match.groups():
                    if match is not None:
                        city = match
                        break
                log('Found city {0}'.format(city), 'debug')
                log('Using state {0}'.format(state), 'debug')
                weather.get_weather_conditions(bot, '', city, state, service)
            except Exception, e:
                msg = ['Found what appears to be a city ({0}) in'.format(city)]
                msg.append('message, but failed to get conditions ')
                msg.append('because of {0}'.format(e))
                log(''.join(msg), 'error')
        elif city_match is not None:
            city = ''
            try:
                for match in city_match.groups():
                    if match is not None:
                        city = match
                        break
                log('Found city {0}'.format(city), 'debug')
                weather.get_weather_conditions(bot, '', city, '', service)
            except Exception, e:
                msg = ['Found what appears to be a city ({0}) in'.format(city)]
                msg.append('message, but failed to get conditions ')
                msg.append('because of {0}'.format(e))
                log(''.join(msg), 'error')
        else:
            try:
                weather.get_weather_conditions(bot, '', '', '', service)
            except Exception, e:
                log(
                    'Failed to get conditions because of {0}'.format(e),
                    'error')
    except Exception, e:
        log(
            'In weather, but an error {0} occurred'.format(e),
            'error')

def help(bot):
    name = bot.name
    log('Entered help', 'debug')
    message = ['\n```\n']
    message.append("You may ask about the weather or what the forecast is:\n")
    message.append("\t\"{0}, what's the weather like in Leander\"\n".format(name))
    message.append("\t\"{0}, how's the weather in Palm Beach, FL?\"\n".format(name))
    message.append("\t\"What's the forecast for Chicago, {0}?\"\n".format(name))
    message.append("You may ask me to google something:\n")
    message.append("\t\"{0}, google 'Why is Jimmy Buffett so awesome' please\"\n".format(name))
    message.append("\t\"{0}, can you please google good restaurants near IBM\"\n".format(name))
    message.append("You may ask me for the latest XKCD comic:\n")
    message.append("\t\"{0}, can you show me the latest xkcd please?\"\n".format(name))
    message.append("You may ask me what RETAIN queue to use for a given subject\n")
    message.append("\t\"{0}, what queue handles SC Notes?\"\n".format(name))
    message.append("You may also use the following shortcuts:\n")
    message.append("\t\"!add word XXX\" where XXX is a word you want to add to the spell checker dictionary\n")
    message.append("\t\"!remove word XXX\" where XXX is a word you want removed from the spell checker dictionary\n")
    message.append("\t\"!add keyword XXX\" where XXX is a term you want to search the news for\n")
    message.append("\t\"!show keywords\" to show all current news keywords\n")
    message.append("\t\"!mute alerts\" to mute severe weather alerts\n")
    message.append("\t\"!unmute alerts\" to unmute severe weather alerts\n")
    message.append("\t\"!show mute status\" to show whether severe weather alerts are muted\n")
    message.append("\t\"!change name XXX\" to change the name I respond to, where XXX is the new name\n")
    message.append("\t\"!show name\" to show my current name\n")
    message.append("\t\"!google XXX\" to search for XXX\n")
    message.append("\t\"!weather [town] [ST]\" get weather for the specified location\n")
    message.append("\t\"!forecast [town] [ST]\" get the forecast for the specified location\n")
    message.append("\t\"!seen XXX\" where XXX is a Space member, to see when they last posted\n")
    message.append("\t\"!rr\" to roll a die\n")
    message.append("\t\"!add new queue 'queue name and description' with keywords 'keyword1...'\" to add a new queue to the database\n")
    message.append("\t\"!add keywords 'keyword[s]' to queue 'queue name'\" to add new keywords to an existing queue\n")
    message.append("\t\"!remove queue 'queue name'\" to remove a queue from the database\n")
    message.append("\t\"!remove keywords 'keyword[s]' from queue 'queue name'\" to remove keywords from an existing queue\n")
    message.append("\t\"!show all queues\" to show all queues\n")
    message.append("\t\"!show queue 'queue name'\" to show a queue's details\n")
    message.append("You may *not* talk disrespectfully to me...")
    message.append('\n```\n')
    bot.sendMessage(''.join(message), bot.std_color)
    return ''.join(message)

def log(message, level):
    '''This allows us to see what function we were in when we got called
    '''
    func = inspect.currentframe().f_back.f_code
    numeric_level = getattr(logging, level.upper(), None)
    logger.log(numeric_level, '%s: %s in %s:%i' % (
        message,
        func.co_name,
        func.co_filename,
        func.co_firstlineno
    ))
