import time

general_insult_list = [
    "are you on drugs?",
    "your mind just hasn't been the same since the electro-shock, has it?",
    "I'm not saying I hate you, but I would unplug your life support to charge my phone.",
    "roses are red, violets are blue, I have 5 fingers, the 3rd ones for you.",
    "I wasn't born with enough middle fingers to let you know how I feel about you.",
    "I bet your brain feels as good as new, seeing that you never use it.",
    "you're so ugly, when your mom dropped you off at school she got a fine for littering.",
    "your birth certificate is an apology letter from the condom factory.",
    "you must have been born on a highway because that's where most accidents happen.",
    "you're so ugly, you could be a modern art masterpiece.",
    "if laughter is the best medicine, your face must be curing the world.",
    "I'd like to see things from your point of view but I can't seem to get my head that far up my butt.",
    "if you're gonna be a smartass, first you have to be smart. Otherwise you're just an ass.",
    "it's better to let someone think you are an idiot than to open your mouth and prove it.",
    "somewhere out there is a tree, tirelessly producing oxygen so you can breathe. I think you owe it an apology.",
    "I don't exactly hate you, but if you were on fire and I had water, I'd drink it.",
    "you are nothing but an unorganized grabastic piece of amphibian crap!",
    "well I could agree with you, but then we'd both be wrong.",
    "you have two brains cells, one is lost and the other is out looking for it.",
    "if I gave you a penny for your thoughts, I'd get change.",
    "I'll never forget the first time we met, although I'll keep trying.",
    "you are proof that God has a sense of humor.",
    "you're as bright as a black hole, and twice as dense.",
    "if you spoke your mind, you'd be speechless.",
    "shock me, say something intelligent.",
    "so a thought crossed your mind? Must have been a long and lonely journey.",
    "I've been called worse things by better people",
    "if what you don't know can't hurt you, you're invulnerable.",
    "100,000 sperm, you were the fastest?",
    "you are depriving some poor village of its idiot.",
    "you speak an infinite deal of nothing",
    "what language are you speaking? Cause it sounds like bullcrap.",
    "hell is wallpapered with all your deleted selfies.",
    "you're the reason the gene pool needs a lifeguard.",
    "you're so fake, Barbie is jealous.",
    "I see the screw-Up fairy has visited us again!",
    "calling you an idiot would be an insult to all stupid people.",
    "you are living proof that manure can sprout legs and walk.",
    "I don't think you are stupid. You just have a bad luck when thinking.",
    "stupidity is not a crime so you are free to go.",]

spelling_insult_list = [
    "where did you learn to type?",
    "my pet ferret can type better than you!",
    "you type like i drive.",
    "do you think like you type?",
    "maybe if you used more than just two fingers...",
    "I've seen penguins that can type better than that.",
    "have you considered trying to match wits with a rutabaga?",
    "I see the screw-Up fairy has visited us again!",
    "I don't think you are stupid. You just have a bad luck when thinking.",
    "are you on drugs?",
    "your mind just hasn't been the same since the electro-shock, has it?",
    "you speak an infinite deal of nothing",
    "if I had a dollar for every time you typed something correctly, I'd be broke.",
    "what language are you speaking? Cause it sounds like bullcrap",
    "it's scary to think that people like you are graduating from college.",
    "you're as sharp as a bowling ball.",
    "you're a few clowns short of a circus.",
    "Your so dense, light must bend around you.",
    ]

bing_insult_list = [
    "Microsoft launched Bing in 2009 with the tagline \"It's not just a search engine, it's a decision engine\"!\n And believe me it helped me a lot to decide why I should stick to Google",
    "*Bill Gates*: So why don't you tell me why Bing failed.\n *Board*: We feel there was a public nescience towards Bing.\n *Bill Gates*: Nescience? Let me Goog- Oh I see what you mean.",
    "For the first time in, oh, a decade, I think, something from Microsoft shipped on time: Jennifer Katharine Gates, weighed 8 pounds 6 ounces when she was downloaded, er, born on Friday, April 26, at 6:11 pm.\nAnd what do Baby Gates and Daddy's products have in common?\n1. Neither can stand on its own two feet without A LOT of third party support.\n2. Both barf all over themselves regularly.\n3. Regardless of the problem, calling Microsoft Tech Support won't help.\n4. As they mature, we pray that they will be better than that which preceeded them.\n5. At first release they're relatively compact, but they seem to grow and grow and grow with each passing year.\n6. Although announced with great fanfare, pretty much anyone can produce one.\n7. They arrive in shaky condition with inadequate documentation.\n8. No matter what, it takes several months between the announcement and the actual release.\n9. Bill gets the credit but someone else did most of the work.\n10. For at least the next year, they'll suck.",
    "Computers are like air conditioners: they stop working properly when you open windows.",
    "I'd say the probability of Windows containing a backdoor is about the same a spreadsheet containing a flight simulator. -- Phil Hunt",
    "ROSWELL, N.M. (AP) -- Today, the United States Air Force issued a long-awaited report about the \"Roswell Incident\" in which some people claim that software from Microsoft functioned correctly in Roswell, New Mexico in 1947. As expected, the government's 261-page report denied that there had ever been any evidence that this had ever happened, despite eyewitness reports to the contrary. The report claims that what witnesses actually saw was an experimental Macintosh running a variation of Unix, or perhaps an experimental Unix machine using a form of the MacOS. Although the official Air Force position is that this is their final report on the matter, long-time Microsoft devotees are not satisfied. \"We know it really happened,\" said Gil Bates, spokesman for a group of Microsoft enthusiasts who call themselves \"The .exe-files\". The group's claim of having seen Windows run without crashing is tainted by the revelation earlier this year that some members had falsified evidence by doctoring output from standard Unix utilities and passing it off as authentic Windows data files.",
    "Q. How many Microsoft programmers does it take to change a lightbulb?\nA. None. Bill Gates will just redefine Darkness(TM) as the new industry standard.",
    "Q. How many Microsoft employees does it take to change a light bulb?\nA. Four. One to change it, one to rewire the socket so that Netscape light bulbs won't work in it, one to rewrite Sun's light bulbs into something unrecognizable (and non-functional), and one to convince the justice department that all Microsoft light bulbs are conforming to anti-trust laws.",
    "Q. How many MicroSoft vice presidents does it take to change a light bulb?\nA. Eight. One to work the bulb, and seven to make sure that MicroSoft gets $2 for every light bulb ever changed anywhere in the world.",
    "God was fed up. In a crash of thunder he yanked up to Heaven three influential humans: Bill Clinton, Boris Yeltsin and Bill Gates. \"The human race is a complete disappointment,\" God boomed. \"You each have one week to prepare your followers for the end of the world.\" With another crash of thunder they found themselves back on Earth.\nClinton immediately called his cabinet. \"I have good news and bad news,\" he announced grimly. \"The good news is that there is a god. The bad news is, God's really mad and plans to end the world in a week.\"\nIn Russia, Yeltsin announced to parliament, \"Comrades, I have bad news and worse news. The bad news is that we were wrong: there is a god after all. The worse news is God's mad and is going to end the world in a week.\"\nMeanwhile, Bill Gates called a meeting of his top engineers. \"I have good news and better news. The good news is that God considers me one of the three most influential men on Earth,\" he beamed. \"The better news is we don't have to fix the bugs in Windows 95.\"",
    "Eventually Bill Gates dies and arrives at heaven where St. Peter awaited him. St. Peter greeted him, with an embarrassed look on his face. \"I don't know what to do with you Bill. You put a PC in every household, but on the other hand you also made Windows 95. So I tell you what, I'll give you the choice between heaven and hell.\"\nBill answers him, \"Really, I can choose?\"\n\"Yes, pick one\" Peter says.\nBill, who is very cautious, says \"Ok, can I visit both before deciding?\"\n\"Ok, that's fair. Where do you want to start?\"\nBill replied, \"Why not start with hell?\"\nSo they both went to hell, which was magnificient. Great beaches, plenty of sun and naked women everywhere, all of whom were smiling at Bill.\n\"That looks wonderfull!\" Bill says. \"Now how about heaven?\"\nSo they went to heaven, which was also magnificient. Great beaches, plenty of sun, but no naked women anywhere.\n\"Ok\", Bill says, \"I pick hell then.\"\nAfter a week, St. Peter decided to visit Bill. The poor guy was on the floor, screaming, and scratching the ground with his nails. He screamed at St. Peter,\n\"No no no! I can't stand it anymore!\"\n\"What's wrong?\", St. Peter asked.\nBill yells \"I don't understand, there is nothing like what I saw the first time! Where is the beach? The naked women?\"\nSt. Peter replied with a wicked smile, \"That was just a demo, Bill.\"",
    "No, Windows is not a virus. Here's what viruses (viri?) do:\n1.They replicate quickly -- okay, Windows does that.\n2.Viruses use up valuable system resources, slowing down the system as they do so -- okay, Windows does that.\n3.Viruses will, from time to time, trash your hard disk -- okay, Windows does that, too.\n4.Viruses are usually carried, unknown to the user, along with valuable programs and systems. Sigh... Windows does that, too.\n5.Viruses will occasionally make the user suspect their system is too slow (see 2) and the user will buy new hardware. Yup, that's with Windows, too.\n\nUntil now it seems Windows is a virus but there are fundamental differences: Viruses are well supported by their authors, are running on most systems, their program code is fast, compact and efficient and they tend to become more sophisticated as they mature.\n\nSo, Windows is not a virus.",
    "Dear Abby -\n\nI am a Vietnam-era deserter from the U. S. Army, and I have a second cousin who works for Microsoft. My mother peddles Nazi hate literature to Girl Scouts and my father (a former dentist) is in jail for 30 years for raping most of his patients while they were under anesthesia. The sole supports of our large family, including myself and my $500-a-week heroin habit, are my uncle (master pick-pocket Benny \"The Fingers\") and my aunt and kid sisters, who are well-known street walkers.\n\nMy problem is this: I have just gotten engaged to the most beautiful, sweetest girl in the world. She is just sweet sixteen, and we are going to marry as soon as she can escape from reform school. To support ourselves, we are going to move to Mexico and start a fake Aztec souvenir factory staffed by child labor. We look forward to bringing our kids into the family business. But -- I am worried that my family will not make a good impression on hers, once she has a chance to meet them.\n\nIn your opinion, Abby: Should I -- or shouldn't I -- let her know about my second cousin who works for Microsoft?\n\nRegards,\n",
    ]

def get_insult(category="general_insults"):
    '''Eventually, this will accpet multiple type of insult requests.
    For example, insults on typing ability, insults on coding ability
    etc.
    '''
    if category == "general_insults":
        category = general_insult_list
    elif category == "spelling_insults":
        category = spelling_insult_list
    elif category == "bing_insults":
        category = bing_insult_list
    else:
        category = general_insult_list
    return category[int(round(time.time() * 1000)) % len(category)]
