import feedparser
import ConfigParser
import threading
import time
import datetime
import pytz
import re
import logging
import inspect
from HTMLParser import HTMLParser
from dateutil import parser

from travis_exceptions import *
from quotes import search_quotes

logger = logging.getLogger("Travis.news")

def log(message, level):
    '''This allows us to see what function we were in when we got called
    '''
    func = inspect.currentframe().f_back.f_code
    numeric_level = getattr(logging, level.upper(), None)
    logger.log(numeric_level, '%s: %s in %s:%i' % (
        message,
        func.co_name,
        func.co_filename,
        func.co_firstlineno
    ))


class News():
    def __init__(self, bot):
        # set instance variables
        self.bot = bot
        config = ConfigParser.ConfigParser()
        try:
            config.readfp(open(bot.config_file))
            self.news_url = config.get('news', 'news_url')
            self.news_urls = config.get('news', 'news_urls')
            self.news_title = config.get('news', 'news_title')
            self.interval = int(config.get('news', 'interval'))
            self.periodic_interval = int(config.get('news', 'periodic_interval'))
            self.news_quantity = int(config.get('news', 'news_quantity'))
            keywords = config.get('news', 'keywords')
            self.keywords = keywords.split()
            periodic = config.get('news', 'periodic')
            self.periodic = periodic.lower() == 'yes' or periodic.lower() == 'true'
            watch = config.get('news', 'watch')
            self.watch = watch.lower() == 'yes' or watch.lower() == 'true'
            self.kw_quantity = int(config.get('news', 'kw_quantity'))
            monitor = config.get('news', 'monitor')
            self.monitor = monitor.lower() == 'yes' or monitor.lower() == 'true'
            # A list of rss entries, consisting of title and url
            entries = re.split('\n', self.news_urls)
            # A dictionary of rssTitle key with rssUrl value
            sites = {}
            # A list of rss_feed instances
            self.rss_feeds = []

            for entry in entries:
                tup = entry.split('@')
                sites[tup[0]] = tup[1]
            for site in sites:
                try:
                    feed = rss_feed(site, sites[site])
                    self.rss_feeds.append(feed)
                except RssError, e:
                    if site is None:
                        site = 'No site'
                    error = ['Could not populate rss feed {0}'.format(site)]
                    error.append(' because {0}'.format(e))
                    log(''.join(error), 'error')
        except IOError, e:
            log('Failed to open config file because: {0}'.format(e), 'error')
            raise InitializationError()
        except ConfigParser.NoSectionError, e:
            log('Failed to locate section in config file: {0}'.format(e), 'error')
            raise InitializationError()
        except ConfigParser.NoOptionError, e:
            log('Failed to locate option {0} in config file'.format(e), 'error')
            raise InitializationError()
        except Exception, e:
            log('Unexpected error {0}'.format(e), 'error')
            raise InitializationError()

        if self.periodic:
            # Start spamming
            t = threading.Thread(name='PeriodicNewsThread',
                    target=self.spamNews,
                    args=(self.periodic_interval,))
            t.daemon = True
            t.start()
        if self.watch:
            # Start spamming
            t = threading.Thread(name='WatchNewsThread',
                    target=self.watch_news)
            t.daemon = True
            t.start()


    def getNews(self):
        feed = rss_feed(self.news_title, self.news_url)
        entries = feed.entries[:self.news_quantity]
        now = datetime.datetime.now()
        timezone = pytz.timezone(self.bot.timezone)
        # Convert to 'aware' datetime obj
        now = timezone.localize(now)
        self.bot.sendMessage(now.strftime("It's %I:%M %p %Z, here's the latest from {0}".format(self.news_title)), self.bot.news_color)
        # Avoid trying to send too quickly
        time.sleep(1)
        for entry in entries:
            self.bot.sendMessage(feed.print_news(entry), self.bot.news_color)
            # Avoid trying to send too quickly
            time.sleep(2)

    def spamNews(self, interval):
        log('Interval : {0}'.format(interval), 'debug')
        while 1:
            # Is it time to fire?
            minute = int(datetime.datetime.now().strftime('%-M'))
            if minute % interval == 0:
                self.getNews()
            # Check once a minute
            time.sleep(60)

    def watch_news(self):
        log('Entered watch_news', 'info')
        while 1:
            log('Entered news_watch while loop', 'debug')
            keywords = self.keywords
            if self.monitor:
                try:
                    new_keywords = self.get_rolling_keywords()
                except Exception, e:
                    log('Error getting rolling keywords because {0}'.format(e), 'error')
                if new_keywords:
                    # search rss feed for last 24 hours
                    for feed in self.rss_feeds:
                        try:
                            feed.update_feed(1440, self.bot.timezone)
                        except RssUpdateError, e:
                            error = ['Could not update rss feed {0}'.format(feed.title)]
                            error.append(' because {0}'.format(e))
                            log(''.join(error), 'error')
                    self.search_feed(self.rss_feeds, new_keywords)
                    quote = search_quotes(new_keywords)
                    if quote:
                        log('Got a quote: {0}'.format(quote), 'debug')
                        self.bot.sendMessage(quote, self.bot.news_color)
            # Set rss feeds to have only most current items
            for feed in self.rss_feeds:
                try:
                    feed.update_feed(self.interval, self.bot.timezone)
                except RssUpdateError, e:
                    error = ['Could not update rss feed {0}'.format(feed.title)]
                    error.append(' because {0}'.format(e))
                    log(''.join(error), 'error')
            # Search for new items matching config keywords
            self.search_feed(self.rss_feeds, keywords)
            time.sleep(self.interval * 60)

    def search_feed(self, rss_feeds, keywords):
        log('Searching news with keywords: {0}'.format(keywords), 'debug')
        for feed in rss_feeds:
            log('Searching feed {0}'.format(feed.title), 'debug')
            try:
                new_items = feed.filter(keywords)
                if new_items:
                    message = "*Fresh from {0}:*\n".format(feed.title)
                    self.bot.sendMessage(message, self.bot.news_color)
                    for item in new_items:
                        self.bot.sendMessage(feed.print_news(item),
                                self.bot.news_color)
            except RssFilterError, e:
                error = ['Problem filtering rss feed {0}'.format(feed.title)]
                error.append(' because {0}'.format(e))
                log(''.join(error), 'error')
            except RssPrintError, e:
                error = ['Problem printing an entry from rss feed {0}'.format(feed.title)]
                error.append(' because {0}'.format(e))
                log(''.join(error), 'error')
            except Exception, e:
                log('Unexpected error {0}'.format(e), 'debug')

    def get_rolling_keywords(self):
        """Get the current list of keywords we've snarfed up
        in the last (interval) minutes
        """
        log('Entered get_rolling_keywords', 'info')
        log('Current interval is {0} minutes'.format(self.interval), 'info')
        curr = self.bot.rolling_keywords
        log('Current set of keywords: {0}'.format(curr), 'debug')
        now = datetime.datetime.now()
        potential_keywords = []
        to_be_deleted = []
        self.bot.lock.acquire()
        for entry in curr:
            # entry is the timestamp we added the keyword
            diff = now - entry
            age = diff.days * 86400 + diff.seconds
            log('Age of entry{0}: {1}'.format(entry, age), 'debug')
            # We only look at last X minutes
            if age < self.interval * 60:
                log('Appending {0}'.format(curr[entry]), 'debug')
                potential_keywords.append(curr[entry])
            else:
                log('Marking {0} for deletion'.format(curr[entry]), 'debug')
                # Can't delete outright because it'll screw up the loop
                to_be_deleted.append(entry)
        self.bot.lock.release()
        for key in to_be_deleted:
            log('Deleting {0}'.format(curr[key]), 'debug')
            del curr[key]
        # Now we have a list of the keywords received in the last X minutes.
        # Next, let's select the ones mentioned more than X times
        qualifying_keywords = []
        for word in potential_keywords:
            if potential_keywords.count(word) >= self.kw_quantity:
                # Let's not add twice
                log('found qualifying kw: {0}'.format(word), 'debug')
                if word not in qualifying_keywords:
                    log('and appended it', 'debug')
                    qualifying_keywords.append(word)
        log('Returning keywords: {0}'.format(qualifying_keywords), 'debug')
        return qualifying_keywords



class rss_feed():
    def __init__(self, title, urlval):
        log('Entered rss_feed init', 'info')
        try:
            self.title = title
            self.urlval = urlval
            feed = feedparser.parse(urlval)
            self.entries = feed['items']
            self.validate_feed_entry(self)
        except Exception, e:
            raise RssError(e)

    def filter(self, keywords):
        """keywords is a list.
        Returns a list of feed entries that match
        """
        log('Entered filter', 'info')
        log('Provided keywords are: {0}'.format(keywords), 'debug')
        time.sleep(2)
        try:
            relevant_results = [(entry) for entry in self.entries for keyword in keywords if keyword.upper() in entry['summary'].upper()]
        except Exception, e:
            raise RssFilterError(e)
        # In case we hit on more than one keyword per article
        relevant_results = list(set(relevant_results))
        if not relevant_results:
            log('Found no relevant news items', 'debug')
        return relevant_results

    def update_feed(self, delta, timezone):
        """delta is how many minutes since last check
        timezone is current timezone
        Returns a list of recent entries
        """
        log('Entered update_feed with delta {0}'.format(delta), 'info')
        try:
            feed = feedparser.parse(self.urlval)
            current_entries = feed['entries']
            timezone = pytz.timezone(timezone)
            now = timezone.localize(datetime.datetime.now())
            updated_feed = []
            for entry in current_entries:
                try:
                    published = parser.parse(entry.published)
                except:
                    # No publish date, so we skip it
                    continue
                difference = now - published
                age = difference.days * 1440 + difference.seconds / 60
                if age < delta:
                    updated_feed.append(entry)
            self.entries = updated_feed
        except Exception, e:
            raise RssUpdateError(e)

    def print_news(self, entry):
        """entry should be a single rss item
        """
        try:
            title = entry.title.encode('utf-8')
            link = entry.link.encode('utf-8')
            summary = re.sub(r'[\t\n]+', '',
                    self.strip_tags(entry.summary)).encode('utf-8')
            message = "[{0}]({1}) - _{2}_".format(title, link, summary)
            return ''.join(message)
        except Exception, e:
            raise RssPrintError(e)

    def validate_feed_entry(self, feed):
        """Accepts a feed instance and validates it has
        title, summary, link and published keys.
        """
        entry = feed.entries[0]
        try:
            title = entry.title
            link = entry.link
            summary = entry.summary
            published = entry.published
        except KeyError, e:
            raise KeyError('Feed does not have key: {0}'.format(e))

    def strip_tags(self, html):
        s = MLStripper()
        s.feed(html)
        return s.get_data()


# Lifted from http://stackoverflow.com/questions/753052/strip-html-from-strings-in-python
class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)
