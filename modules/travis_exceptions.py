class InitializationError(Exception):
    pass

class ParseError(Exception):
    pass

class RssError(Exception):
    pass

class RssPrintError(Exception):
    pass

class RssFilterError(Exception):
    pass

class RssUpdateError(Exception):
    pass
