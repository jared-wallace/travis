import ConfigParser
import requests
import json
import logging
import inspect

class search():
    def __init__(self, config_file):
        self.logger = logging.getLogger("Travis.search")
        config = ConfigParser.ConfigParser()
        try:
            config.readfp(open(config_file))
            self.key = config.get('google', 'google_key')
            self.engine = config.get('google', 'google_engine_id')
            self.quantity = int(config.get('google', 'quantity'))
        except IOError, e:
            self.log('Failed to open config file because: {0}'.format(e), 'error')
            raise InitializationError
        except ConfigParser.NoSectionError, e:
            self.log('Failed to locate section in config file: {0}'.format(e), 'error')
            raise InitializationError
        except ConfigParser.NoOptionError, e:
            self.log('Failed to locate option {0} in config file'.format(e), 'error')
            raise InitializationError
        except Exception, e:
            self.log('Unexpected error {0}'.format(e), 'error')
            raise InitializationError

    def log(self, message, level):
        '''This allows us to see what function we were in when we got called
        '''
        func = inspect.currentframe().f_back.f_code
        numeric_level = getattr(logging, level.upper(), None)
        self.logger.log(numeric_level, '%s: %s in %s:%i' % (
            message,
            func.co_name,
            func.co_filename,
            func.co_firstlineno
        ))

    def search_google(self, term):
        '''Return a dictionary of relevant info:
        'list': list of X results from search of 'term', each result being formatted nicely.
        'spelling': dictionary containing mispelled, corrected_term
        'meta': dictionary containing quantity, total_results, search_time
        '''
        self.log('Entered search_google', 'info')
        url = 'https://www.googleapis.com/customsearch/v1'
        payload = {
            'key': self.key,
            'cx': self.engine,
            'q': term}
        req = requests.get(url, params=payload)
        body = json.loads(req.text)
        self.log('Received from Google: {0}'.format(body), 'debug')
        if req.status_code == requests.codes.ok:
            results = []
            for i in range (0, self.quantity):
                link_text = body['items'][i]['title'].encode('utf-8')
                link = body['items'][i]['link'].encode('utf-8')
                snippet = ''.join(body['items'][i]['snippet'].encode('utf-8').splitlines())
                txt = "[{0}]({1}) {2}".format(
                        link_text,
                        link,
                        snippet)
                results.append(txt)
            if 'spelling' in body:
                spelling  = {'misspelled': True}
                spelling['corrected_term'] = body['spelling']['correctedQuery'].encode('utf-8')
            else:
                spelling = {'misspelled': False}
            meta = {'quantity': self.quantity}
            meta['total_results'] = body['searchInformation']['formattedTotalResults'].encode('utf-8')
            meta['search_time'] = body['searchInformation']['formattedSearchTime'].encode('utf-8')

            ret = {'list': results, 'spelling': spelling, 'meta': meta}
            self.log('Returning result: {0}'.format(ret), 'debug')
            return ret
        else:
            self.log('Got bad response from google', 'error')





