import ConfigParser
import requests
import time
import json
import logging
import threading
import inspect
import re

from travis_exceptions import InitializationError

wu_weather_api_url = 'http://api.wunderground.com/api/'
logger = logging.getLogger("Travis.weather")

class Weather():
    def __init__(self, bot):
        log('Initializing weather...', 'info')
        # Load relevant config
        config = ConfigParser.ConfigParser()
        config.readfp(open(bot.config_file))
        try:
            alerts = config.get('weather', 'weather_alerts').lower()
            if alerts == 'yes' or alerts == 'true':
                self.weather_alerts = True
            else:
                self.weather_alerts = False
            self.wu_weather_token = config.get('weather', 'wu_weather_token')
            refresh = int(config.get('weather', 'weather_refresh'))
            self.color = config.get('weather', 'color')
            if refresh < 600:
                # API limit is 500 calls per day. 600 here means max of 144
                # calls per day for the alert function
                self.refresh = 600
            else:
                self.refresh = refresh
            self.zipcode = config.get('weather', 'zipcode')
        except IOError, e:
            log('Failed to open config file because: {0}'.format(e), 'error')
            raise InitializationError
        except ConfigParser.NoSectionError, e:
            log('Failed to locate section in config file: {0}'.format(e), 'error')
            raise InitializationError
        except ConfigParser.NoOptionError, e:
            log('Failed to locate option {0} in config file'.format(e), 'error')
            raise InitializationError
        except Exception, e:
            log('Unexpected error {0}'.format(e), 'error')
            raise InitializationError
        self.bot = bot
        self.mute = False

        # If alerts are not enabled, we're done.
        if self.weather_alerts:
            # Enter our monitoring loop
            t = threading.Thread(name='WeatherThread',
                    target=self.monitor_for_alerts)
            t.daemon = True
            t.start()

    def monitor_for_alerts(self):
        # We set alert location based on default zip in file for now
        req = requests.get('http://autocomplete.wunderground.com/aq?query=' + self.zipcode)
        loc = json.loads(req.text)['RESULTS'][0]['l'].encode('utf-8')
        req = requests.get(wu_weather_api_url + self.wu_weather_token + '/conditions' + loc + '.json')
        t = json.loads(req.text)
        self.default_location = t['current_observation']['display_location']['full'].encode('utf-8')
        while 1:
            log('Getting current alerts', 'info')
            get_weather_conditions(self.bot, '', '', '', 'alerts')
            time.sleep(self.refresh)


def log(message, level):
    '''This allows us to see what function we were in when we got called
    '''
    func = inspect.currentframe().f_back.f_code
    numeric_level = getattr(logging, level.upper(), None)
    logger.log(numeric_level, '%s: %s in %s:%i' % (
        message,
        func.co_name,
        func.co_filename,
        func.co_firstlineno
    ))


def get_location(bot, zipcode='', city='', state=''):
    try:
        w = bot.weather_object
    except Exception, e:
        log("Can't get location because {0}".format(e), 'error')
        return
    if zipcode == '' and city == '':
        # fall back to cfg loaded zip
        log('Falling back to default zip', 'debug')
        zipcode = w.zipcode
    if zipcode == '':
        if state:
            city = city + ', ' + state
        log('Using city {0}'.format(city), 'debug')
        # use wu autocomplete to get best loc string
        req = requests.get('http://autocomplete.wunderground.com/aq?query=' + city)
    else:
        req = requests.get('http://autocomplete.wunderground.com/aq?query=' + zipcode)
    log('Got options: {0}'.format(req.text), 'debug')
    try:
        loc = json.loads(req.text)['RESULTS'][0]['l'].encode('utf-8')
    except:
        message  = "Apologies old chap, I'm afraid I can't find any information for {0}".format(city)
        if not w.mute:
            bot.sendMessage(message, w.color)
        return
    return loc

def get_weather_conditions(bot, zipcode='', city='', state='', service='conditions'):
    """Service can be: 'conditions', 'forecast' or 'forecast10day
    """
    try:
        w = bot.weather_object
    except Exception, e:
        log("Can't get conditions because {0}".format(e), 'error')
        return
    log('Entered get_weather_conditions', 'info')
    log('zip: {0}, city: {1}, state: {2}, service: {3}'.format(zipcode, city, state, service), 'debug')
    loc = get_location(bot, zipcode, city, state)
    req = requests.get(wu_weather_api_url + w.wu_weather_token + '/' + service + loc + '.json')

    t = json.loads(req.text)
    log('Response from WU was: {0}'.format(t), 'debug')
    if service == 'conditions':
        location = t['current_observation']['display_location']['full'].encode('utf-8')

        temp = t['current_observation']['temp_f']
        temp = int(round(temp))
        log('var: {0}'.format(temp), 'debug')

        wind_dir = t['current_observation']['wind_dir'].encode('utf-8')
        log('var: {0}'.format(wind_dir), 'debug')

        wind_speed = t['current_observation']['wind_mph']
        wind_speed = int(round(wind_speed))
        log('var: {0}'.format(wind_speed), 'debug')

        feelslike_f = t['current_observation']['feelslike_f']
        feelslike_f = int(round(float(feelslike_f)))
        log('var: {0}'.format(feelslike_f), 'debug')

        conditions = t['current_observation']['weather'].encode('utf-8')
        log('var: {0}'.format(location), 'debug')

        windchill = temp != feelslike_f
        wind = wind_speed > 0

        message = ["It's {0}".format(conditions.lower())]
        if not wind:
            message.append(" and calm")
        message.append(" in {0}.".format(location))
        message.append(" The current temperature is {0}".format(temp))
        if wind:
            if windchill:
                message.append(", and")
            message.append(" with wind out of the {0}".format(wind_dir))
            message.append(" at {0}".format(wind_speed))
            if wind_speed != 1:
                message.append(" miles")
            else:
                message.append(" mile")
            message.append(" per hour")
            if windchill:
                message.append(", it feels like {0}".format(feelslike_f))
        message.append(".")
    elif service == 'forecast':
        # No way around making a double call to the API. The forecast result
        # does not include a string with city/state :/
        req2 = requests.get(wu_weather_api_url + w.wu_weather_token + '/conditions' + loc + '.json')
        t2 = json.loads(req2.text)
        location = t2['current_observation']['display_location']['full'].encode('utf-8')
        message = []
        bot.sendMessage('*Forecast for {0}:*'.format(location), w.color)
        for period in t['forecast']['txt_forecast']['forecastday']:
            msg = ['*']
            msg.append(period['title'])
            msg.append('*: _')
            msg.append(period['fcttext'])
            msg.append('_\n')
            message.append(''.join(msg))
    elif service == 'alerts':
        message = []
        location = w.default_location
        if 'alerts' in t:
            quantity = len(t['alerts'])
            if quantity > 0:
                log('Found severe weather alert', 'info')
                if quantity > 1:
                    message.append('*Severe weather alerts for ')
                else:
                    message.append('*Severe weather alert for ')
                message.append(w.default_location)
                message.append('*\n\n')
                if not w.mute:
                    bot.sendMessage(''.join(message), bot.err_color)
                message = []
                for alert in t['alerts']:
                    message.append('*')
                    message.append(alert['description'])
                    message.append('*\n')
                    message.append(re.sub(r'\\u000A','\n',alert['message']))
                    message.append('\n')
            else:
                return
        else:
            return



    if not w.mute or service != 'alerts':
        if service == 'alerts':
            color = bot.err_color
        else:
            color = w.color
        bot.sendMessage(''.join(message), color)
        message = '(weather information courtesy of Weather Underground)'
        bot.sendMessage(message, color)


