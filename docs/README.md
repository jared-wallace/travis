# TravisMcGee
General purpose bot for IBM Workspace
Version 0.5

### Purpose
This is mainly an excuse to play around with the Workspace API

### Current functionality
Travis can be set to span the top X headlines every X amount of time, taken from the X rss feed. The values are taken from the "travis.cfg" file.

Travis can listen for both git commits and git deployments (via git hooks) and send notifications to the space.

Writing "Travis, what's the latest xkcd" in the channel will provide you the link to the latest XKCD comic.

You can ask Travis what the weather is like, using a zipcode or a city (Travis will do best guess on state if you pick city.
ex. "Travis, what's the weather like in Leander?" or "Travis, how's the weather in 90210?"

Travis is multithreaded.

Travis logs to the file "travis.log"

### Planned functionality
See the "plans.txt" file

### Immediate focus
Exploring the callback API's

### Contributing

**Master** branch - production ready

**Development** branch - latest delivered changes
* nightly builds come from here
* when code reaches stability, merged into master and release tagged

Feature branches
* branch off from **development**
* merge into **development**
* naming convention - anything except **master**, **development**, release-* or hotfix-*
* used to develop new features for an upcoming release
* exists as long as a feature is in development, then either merged into **development** or discarded
* usually won't be present in the master repository(IBM GH)
* creating:
  * git checkout -b myfeature development
* merging:
  * git checkout development
  * git merge --no-ff myfeature
  * git branch -d myfeature
  * git push origin development

Release branches
* branch off from development
* merge into **development** and **master**
* naming convention - release-*
* supports preparation of a new production release
* allows minor bug fixes and preparing meta-data for a release
* should be branched off **development** when it's ready (or almost ready) for release
* the branching marks the time when an upcoming release gets assigned a version number
* creating:
  * git checkout -b release-1.2 development
  * ./bump-version.sh 1.2
  * git commit -a -m "Bumped version number to 1.2"
* merging:
  * git checkout master
  * git merge --no-ff release-1.2
  * git tag -a 1.2
  * git push origin 1.2
  * git checkout development
  * git merge --no-ff release-1.2 (probably gonna have a merge conflict, so just fix it)
  * git branch -d release-1.2


Hotfix branches
* branch off from master
* merge into **development** and **master**
* naming convention - hotfix-*
* similar in purpose to release branches, but unplanned
* for fixing critical bugs
* creating:
  * git checkout -b hotfix-1.2.1 master
  * ./bump-version.sh 1.2.1
  * git commit -a -m "Bumped version number to 1.2.1"
* fix the bug with one or more commits
  * git commit -m "Fixed security hole CV-111111"
* merging:
  * git checkout master
  * git merge --no-ff hotfix-1.2.1
  * git tag -a 1.2.1
  * git checkout development
  * git merge --no-ff hotfix-1.2.1
  * Exception to merging into development is when a release branch exists. In that case, merge into the release.
  * git branch -d hotfix-1.2.1

Creating a cert and key with no passphrase for local testing:
* openssl genrsa -out server.key 1024
* openssl req -new -key server.key -out server.csr
* openssl x509 -req -days 366 -in server.csr -signkey server.key -out server.crt
* openssl x509 -in server.crt -out cert.pem -outform PEM
* The cert is cert.pem, the key is server.key

How to write logging messages
* Levels:
  * CRITICAL - any error that forces shutdown to avoid data loss or prevent further data loss.
  * ERROR - any error fatal to an operation, but not the app. Example, can't open a file.
  * WARNING - anything that could cause odd but recoverable behavior
  * INFO - gernerally useful info, like service start/stop, configuration values, etc
  * DEBUG - When you need to trace a function to find a problematic statement
