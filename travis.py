#!/usr/bin/python

import sys
import os
import logging
from logging.handlers import RotatingFileHandler
import threading
import signal
import re
import threading
from subprocess import check_output

import json
import inspect
import requests
import base64
import time
import feedparser
import ConfigParser
import enchant
from enchant.tokenize import get_tokenizer, EmailFilter, URLFilter

import modules.news as news
import modules.git as git
import modules.travissocket as travissocket
import modules.travis_parse as travis_parse
import modules.weather as weather
import modules.queues as queues
from modules.travis_exceptions import InitializationError

dir_path = os.path.dirname(os.path.realpath(__file__))
_django_path = dir_path + '/travis_django/'
_dev_server_address = _django_path + 'manage.py'


class Travis:

    def reloadTravis(self, signum, frame):
        # save data
        self.queues.save_queues()
        self.sendMessage(
            '*New features incoming...* (_restarting_)', self.std_color)
        os.execv(__file__, sys.argv)

    def __init__(self):
        # Until we read the config, we print to stdout.
        print 'Entered Travis init'
        try:
            self.readConfig()
        except Exception, e:
            # Have to shut down
            msg = ['Problem reading config file because of ']
            msg.append('{0}. Check log for details.'.format(e))
            print ''.join(msg)
            raise SystemExit

        # Replace our secrets in the appropriate files
        webhook_key = self.webhook_key
        sub1 = "s/webhook_key\ =.*/webhook_key\ =\ '" + self.webhook_key + "'/"
        # Gotta escape all the slashes here :/
        self.sock_path = dir_path + '/travis.sock'
        tmp_sock_path = re.sub(r"\/", "\/", self.sock_path)
        sub2 = ["s/server_address\ =.*/server_address\ =\ '"]
        sub2.append(tmp_sock_path + "'/")
        path = _django_path + 'travis_web/views.py'

        try:
            self.log('Adjusting views.py', 'debug')
            check_output(['sed', '-i', sub1, path])
            check_output(['sed', '-i', ''.join(sub2), path])
        except Exception, e:
            self.log(
                'Error adjusting views.py because {0}'.format(e),
                'error')
            msg = [self.name]
            msg.append(' will probably not be listening on the channel')
            self.log(''.join(msg))

        self.adjust_django()
        self.appAccessKey = self.appid + ':' + self.appsecret
        self.auth = base64.b64encode(self.appAccessKey.encode('ascii'))
        # Get first auth token
        data = self.getAuthorized()
        # Send wake up message
        msg = ['_\*yawn\*_. *I could really use some coffee* (_']
        msg.append(self.name)
        msg.append(' is starting up_)')
        self.sendMessage(''.join(msg), self.std_color)
        # Create our rolling keyword structure
        self.rolling_keywords = {}
        self.lock = threading.Lock()
        # Get our queue instance
        self.queues = queues.queue()
        # Load News module if enabled
        if self.news_enabled == 'True':
            try:
                self.news_object = news.News(self)
            except Exception, e:
                self.log(
                    'Error initializing News module because {0}'.format(e),
                    'error')
        # Load git module
        try:
            self.git_object = git.Git(self)
        except Exception, e:
            self.log(
                'Error initializing Git module because {0}'.format(e),
                'error')
        # Load weather module
        try:
            self.weather_object = weather.Weather(self)
        except Exception, e:
            self.log(
                'Error initializing Weather module because {0}'.format(e),
                'error')
        # Start travis socket
        self.log('Starting UDS', 'debug')
        try:
            self.travis_socket = travissocket.TravisSocket(self)
        except OSError, e:
            # This is not fatal to Travis, as the news module
            # will still function. That's pretty much it though.
            self.log(
                'Error initializing the Travis socket because: {0}'.format(e),
                'error')

        # Register signal handler. This allows us a graceful reload
        # when sent a HUP.
        signal.signal(signal.SIGHUP, self.reloadTravis)

        # Spawn a django instance if we're in debug mode
        if self.debug and not self.standalone:
            self.log('In debug mode, trying to spawn Django', 'info')
            # Start Django dev server in new thread
            t = threading.Thread(
                    name='django thread',
                    target=self.start_django)
            t.daemon = True
            t.start()

        # Enter our run loop unless we're in standalone mode
        print 'standalone = {0}'.format(self.standalone)
        if not self.standalone:
            try:
                while 1:
                    time.sleep(60)
                    pass
            except KeyboardInterrupt:
                print '\nShutting down'
                # save data
                self.queues.save_queues()
                msg = ['*Bedtime for bonzo...*']
                msg.append('_({0} is shutting down)_'.format(self.name))
                self.sendMessage(''.join(msg), self.std_color)
                self.log(
                    'Caught keyboard interrupt, shutting down',
                    'info')
                raise SystemExit

    def start_django(self):
        try:
            check_output([_dev_server_address, 'runserver', 'localhost:1337'])
        except Exception, e:
            self.log(
                'Could not start Django because: {0}'.format(e),
                'error')

    def adjust_django(self):
        '''This allows us to maintain secrets solely in the Travis config
        '''
        subs = []
        msg = ["s/SECRET_KEY\ =.*/SECRET_KEY\ =\ '"]
        msg.append(self.django_secret_key)
        msg.append("'/")
        subs.append(''.join(msg))
        # Set Django debug setting to match the Travis config
        if self.debug == 'yes' or self.debug == 'True':
            subs.append('s/DEBUG\ =.*/DEBUG\ =\ True/')
        else:
            subs.append('s/DEBUG\ =.*/DEBUG\ =\ False/')
        msg = ['s/ALLOWED_HOSTS\ =.*/ALLOWED_HOSTS\ =\ ']
        msg.append(self.allowed_hosts)
        msg.append('/')
        subs.append(''.join(msg))
        path = re.sub(r"\/", "\/", self.django_logfile)
        subs.append("s/logfile\ =.*/logfile\ =\ '" + path + "'/")
        path = _django_path + 'travis/settings.py'

        try:
            for sub in subs:
                check_output(['sed', '-i', sub, path])
        except Exception, e:
            self.log(
                'Could not adjust Django settings.py because {0}'.format(e),
                'error')

    def getAuthorized(self):
        try:
            payload = {'grant_type': 'client_credentials'}
            resp = requests.post(
                url='https://api.watsonwork.ibm.com/oauth/token',
                headers={
                    'Authorization': 'Basic %s' % self.auth.decode('ascii'),
                    'content-type': 'application/x-www-form-urlencoded'},
                data=payload)
            self.log(
                'Response to auth token request: {0}'.format(resp.text),
                'debug')
            data = json.loads(resp.text)
            self.token = data['access_token']
            expires = data['expires_in']
            # With a 12 hour timeout, we shouldn't have to re-auth all
            # that often. Setting this allows us to forgo error checking
            # for expired auth, and reduces the times we hit IBM
            self.token_expires = time.time() + expires
            msg = ['Token: {0} expires at '.format(self.token)]
            msg.append('{0}'.format(self.token_expires))
            self.log(
                ''.join(msg),
                'debug')
        except:
            msg = ['Could not get authorized. Network issue, or perhaps the']
            msg.append('appid or appsecret is incorrect.')
            self.log(
                ''.join(msg),
                'debug')

    def log(self, message, level):
        '''This allows us to see what function we were in when we got called
        '''
        func = inspect.currentframe().f_back.f_code
        numeric_level = getattr(logging, level.upper(), None)
        self.logger.log(numeric_level, '%s: %s in %s:%i' % (
            message,
            func.co_name,
            func.co_filename,
            func.co_firstlineno
        ))

    def init_log(self):
        '''Set our log printing format, and make sure Apache can write to it
        '''
        numeric_level = getattr(logging, self.level.upper(), None)
        msg = ['[%(asctime)s]:  [%(levelname)s] ']
        msg.append('(%(threadName)-10s) %(message)s')
        try:
            os.chmod(self.logfile, 0666)
            os.chmod(self.django_logfile, 0666)
        except Exception, e:
            # Obviously, we can't log it normally...
            print 'Error in init log'.format(e)

        self.logger = logging.getLogger("Travis")
        self.logger.setLevel(numeric_level)
        handler = RotatingFileHandler(
            self.logfile,
            maxBytes=500000,
            backupCount=20)
        formatter = logging.Formatter(''.join(msg))
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)


    def sendMessage(self, message, color='#000000'):
        '''General purpose function. Sends what ever string is passed in
        to the appropriate destination.
        '''
        if self.debug:
            # Avoids spamming the space when testing
            print message
        else:
            # Allow two seconds for safety's sake
            if self.token_expires < (time.time() - 2):
                self.getAuthorized()
            try:
                messagePayload = {
                    'type': 'appMessage',
                    'version': 1.0,
                    'annotations': [{
                        'type': 'generic',
                        'color': color,
                        'version': 1.0,
                        'text': message}]}
                msg = 'Sent off {0} to space'.format(messagePayload)
                self.log(msg, 'debug')
                url = ['https://api.watsonwork.ibm.com/v1/spaces/']
                url.append(self.spaceid)
                url.append('/messages')
                headers = {
                        'Authorization': 'Bearer {0}'.format(self.token),
                        'content-type': 'application/json'}
                respMessage = requests.post(
                    url=''.join(url),
                    headers=headers,
                    json=messagePayload)
                try:
                    # Did we get json back?
                    msg = 'Response from IBM was: {0}'.format(respMessage.json)
                    self.log(msg, 'debug')
                except ValueError, e:
                    # Let's default to text then
                    msg = 'Response from IBM was: {0}'.format(respMessage.text)
                    self.log(msg, 'debug')
                finally:
                    # Regardless, get the numerical response code
                    msg = 'Status was: {0}'.format(respMessage.status_code)
                    self.log(msg, 'debug')
            except Exception as e:
                self.log('Error sending message:{0}'.format(e), 'error')


    def readConfig(self):
        '''We start off printing to console, as logging is not yet functional
        '''
        print 'Entered readConfig'
        self.config_file = 'config/travis.cfg'
        config = ConfigParser.ConfigParser()
        config.readfp(open(self.config_file))

        # set log level and format
        try:
            self.level = config.get('global', 'log_level')
            self.logfile = config.get('global', 'logfile')
            self.django_logfile = config.get('global', 'django_logfile')
            print 'Using Travis logfile {0}'.format(self.logfile)
            print 'Using Django logfile {0}'.format(self.django_logfile)
        except Exception, e:
            print 'Using default logfiles because {0}'.format(e)
            self.level = 'info'
            self.logfile = 'logs/travis.log'
            self.django_logfile = 'logs/django.log'
        self.init_log()
        self.log('\n\nTRAVIS is starting up!\n\n', 'info')
        self.log('Logging is now enabled', 'info')
        self.log('Log level is: {0}'.format(self.level), 'info')
        try:
            # Get global settings
            self.appid = config.get('global', 'appid')
            self.appsecret = config.get('global', 'appsecret')
            self.spaceid = config.get('global', 'spaceid')
            self.webhook_key = config.get('global', 'webhook_key')
            self.api_url = config.get('global', 'api_url')
            try:
                self.size_of_die = config.get('global', 'size_of_die')
            except:
                self.size_of_die = 6
            self.name = config.get('global', 'name').capitalize()
            self.default_language = config.get('global', 'default_language')
            self.personal_dictionary = config.get('global', 'personal_dictionary')
            self.d = enchant.DictWithPWL(self.default_language, self.personal_dictionary)
            self.tokenizer = get_tokenizer(self.default_language, [EmailFilter, URLFilter])
            debug = config.get('global', 'debug')
            if debug.lower() == 'yes' or debug.lower() == 'true':
                self.debug = True
            else:
                self.debug = False
            standalone = config.get('global', 'standalone')
            if standalone.lower() == 'yes' or standalone.lower() == 'true':
                self.standalone = True
            else:
                self.standalone = False
            self.tmp_dir = config.get('global', 'tmp_dir')
            self.news_enabled = config.get('global', 'news_enabled')
            self.timezone = config.get('global', 'timezone')
            # Get server specific config
            self.django_secret_key = config.get('server', 'django_secret_key')
            hosts = config.get('server', 'allowed_hosts')
            # Translate into Django ready format
            self.allowed_hosts = str(hosts.split())
            # Get color codes
            self.git_message_color = config.get('git', 'message_color')
            self.git_deploy_color = config.get('git', 'deploy_color')
            self.xkcd_color = config.get('xkcd', 'color')
            self.std_color = config.get('global', 'std_color')
            self.err_color = config.get('global', 'err_color')
            self.news_color = config.get('news', 'color')
            # Get misc settings
            self.insult_threshold = int(config.get('behavior', 'insult_threshold'))
            self.response_lag = int(config.get('performance', 'response_lag'))
        except IOError, e:
            self.log(
                    'Failed to open config file because: {0}'.format(e),
                    'error')
            raise InitializationError()
        except ConfigParser.NoSectionError, e:
            self.log(
                'Failed to locate section in config file: {0}'.format(e),
                'error')
            raise InitializationError()
        except ConfigParser.NoOptionError, e:
            self.log(
                'Failed to locate option {0} in config file'.format(e),
                'error')
            raise InitializationError()
        except Exception, e:
            self.log('Unexpected error {0}'.format(e), 'error')
            raise InitializationError()



def main():
    travis = Travis()


if __name__ == '__main__':
    main()
