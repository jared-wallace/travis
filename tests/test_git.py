import unittest
from subprocess import check_output

import travis
import modules.git as git

path = 'config/travis.cfg'

class GitTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # set to standalone
        sub = "s/standalone\ =.*/standalone\ =\ yes/"
        check_output(['sed', '-i', sub, path])
        cls.t = travis.Travis()

    @classmethod
    def tearDownClass(cls):
        sub = "s/standalone\ =.*/standalone\ =\ no/"
        check_output(['sed', '-i', sub, path])

    def setUp(self):
        check_output(['cp', 'config/travis.cfg', '/tmp/sooper'])

    def test_fail_init(self):
        """ Test initialization with bad config
        """
        sub1 = "s/git_message_color\ =.*//"
        print(check_output(['sed', '-i', sub1, path]))
        with self.assertRaises(travis.InitializationError):
            g = git.Git(self.__class__.t)

    def test_pass_init(self):
        """ Test initialization
        """
        try:
            g = git.Git(self.__class__.t)
        except:
            self.fail("Failed to initialize git")

    def tearDown(self):
        check_output(['cp', '/tmp/sooper', 'config/travis.cfg'])


def suite():
    """ Gather all news tests together in a suite
    """
    return unittest.TestLoader().loadTestsFromTestCase(NewsTest)

def main():
    unittest.main()

if __name__ == '__main__':
    main()
