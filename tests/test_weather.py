import unittest
from subprocess import check_output

import travis
from modules import weather

path = 'config/travis.cfg'

class WeatherTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # set to standalone
        sub = "s/standalone\ =.*/standalone\ =\ yes/"
        check_output(['sed', '-i', sub, path])
        cls.t = travis.Travis()

    @classmethod
    def tearDownClass(cls):
        sub = "s/standalone\ =.*/standalone\ =\ no/"
        check_output(['sed', '-i', sub, path])

    def setUp(self):
        check_output(['cp', 'config/travis.cfg', '/tmp/sooper'])

    def test_fail_init(self):
        """ Test initialization failure
        """
        sub1 = "s/weather_refresh\ =.*//"
        print(check_output(['sed', '-i', sub1, path]))
        with self.assertRaises(travis.InitializationError):
            w = weather.Weather(self.__class__.t)

    def test_init(self):
        """ Test initialization
        """
        try:
            w = weather.Weather(self.__class__.t)
        except:
            self.fail("Failed to initialize Weather")

    def test_get_conditions_with_no_location(self):
        try:
            weather.get_weather_conditions(self.__class__.t)
        except Exception, e:
            self.fail("Failed to get default conditions: {0}".format(e))

    def test_get_forecast_with_no_location(self):
        try:
            weather.get_weather_conditions(self.__class__.t, '', '', '', 'forecast')
        except Exception, e:
            self.fail("Failed to get default forecast: {0}".format(e))

    def test_get_conditions_with_zip(self):
        try:
            weather.get_weather_conditions(self.__class__.t, '23456', '')
        except Exception, e:
            self.fail("Failed to get default conditions: {0}".format(e))

    def test_get_conditions_with_city(self):
        try:
            weather.get_weather_conditions(self.__class__.t, '', 'Austin')
            weather.get_weather_conditions(self.__class__.t, '', 'St. Petersburg')
            weather.get_weather_conditions(self.__class__.t, '', 'San Francisco')
        except Exception, e:
            self.fail("Failed to get default conditions: {0}".format(e))

    def test_get_conditions_with_city_and_state(self):
        try:
            weather.get_weather_conditions(self.__class__.t, '', 'Austin', 'TX')
            weather.get_weather_conditions(self.__class__.t, '', 'Fort Lauderdale', 'FL')
            weather.get_weather_conditions(self.__class__.t, '', 'San Francisco', 'CA')
        except Exception, e:
            self.fail("Failed to get default conditions: {0}".format(e))

    def tearDown(self):
        check_output(['cp', '/tmp/sooper', 'config/travis.cfg'])


def suite():
    """ Gather all news tests together in a suite
    """
    return unittest.TestLoader().loadTestsFromTestCase(NewsTest)

def main():
    unittest.main()

if __name__ == '__main__':
    main()
