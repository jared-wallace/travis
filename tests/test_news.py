import unittest
from subprocess import check_output

import travis
from modules import news

path = 'config/travis.cfg'

class NewsTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # set to standalone
        sub = "s/standalone\ =.*/standalone\ =\ yes/"
        check_output(['sed', '-i', sub, path])
        cls.t = travis.Travis()

    @classmethod
    def tearDownClass(cls):
        sub = "s/standalone\ =.*/standalone\ =\ no/"
        check_output(['sed', '-i', sub, path])

    def setUp(self):
        check_output(['cp', 'config/travis.cfg', '/tmp/sooper'])

    def test_fail_init(self):
        """ Test initialization failure
        """
        sub1 = "s/news_title\ =.*//"
        print(check_output(['sed', '-i', sub1, path]))
        with self.assertRaises(travis.InitializationError):
            news_object = news.News(self.__class__.t)

    def test_init(self):
        """ Test initialization
        """
        try:
            news_object = news.News(self.__class__.t)
        except:
            self.fail("Failed to initialize News")

    def test_bot_assignment(self):
        """News obj should have a Travis instance
        """
        news_obj = news.News(self.__class__.t)
        self.assertIsInstance(news_obj.bot, travis.Travis)

    def tearDown(self):
        check_output(['cp', '/tmp/sooper', 'config/travis.cfg'])


def suite():
    """ Gather all news tests together in a suite
    """
    return unittest.TestLoader().loadTestsFromTestCase(NewsTest)

def main():
    unittest.main()

if __name__ == '__main__':
    main()
