#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import json
from subprocess import check_output

import travis
import modules.travis_parse as travis_parse

path = 'config/travis.cfg'

class ParseTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # set to standalone
        sub = "s/standalone\ =.*/standalone\ =\ yes/"
        check_output(['sed', '-i', sub, path])
        cls.t = travis.Travis()

    @classmethod
    def tearDownClass(cls):
        sub = "s/standalone\ =.*/standalone\ =\ no/"
        check_output(['sed', '-i', sub, path])

    def setUp(self):
        check_output(['cp', 'config/travis.cfg', '/tmp/sooper'])

    def test_parse_type_git_message(self):
        data = """{
            "type": "gitMessage",
            "commit_hash": "1234",
            "author": "Jimmy Buffett",
            "commit_msg": "Wasting away again in Margaritaville"}"""

        result = travis_parse.parse(data, self.__class__.t)
        self.assertEqual(result, 200)

    def test_parse_type_git_deployment(self):
        data = """{
            "type": "gitDeployment",
            "branch": "Havana",
            "to_commit": "1234",
            "deploy_dir": "Daydreaming"}"""
        result = travis_parse.parse(data, self.__class__.t)
        self.assertEqual(result, 200)

    #def test_parse_type_message_created(self):
    #    data = r"""{
    #        "spaceName":"☠Avast!",
    #        "spaceId":"5750732629a6b4d16d65f032",
    #        "messageId":"586d0d33e4b0d2b3b58697ee",
    #        "time":1483541811577,
    #        "type":"message-created",
    #        "userName":"Travis",
    #        "userId":"7cfa90b9-8aa9-4b2f-abf4-42c347fe7d51",
    #        "contentType":"text/html",
    #        "content":"I want the world to burn"}"""
    #    result = travis_parse.parse(data, self.__class__.t)
    #    self.assertEqual(result, 200)

    #def test_help(self):
    #    """Spacing is critical for the ability to match strings.
    #    If the 'message' docstring below has a single space out of place, it
    #    will fail the comparison.
    #    """
    #    name = self.__class__.t.name
    #    data = r"""{
    #        "spaceName":"☠Avast!",
    #        "spaceId":"5750732629a6b4d16d65f032",
    #        "messageId":"686d0d33e4b0d2b3b58697ee",
    #        "time":1483541811577,
    #        "type":"message-created",
    #        "userName":"",
    #        "userId":"7cfa90b9-8aa9-4b2f-abf4-42c347fe7d51",
    #        "contentType":"text/html",
    #        "content":"Travis, what are the right questions?"}"""
    #    data = json.loads(data)
    #    result = travis_parse.command(data, self.__class__.t)
    #    message = ['\n```\n']
    #    message.append("You may ask about the weather:\n")
    #    message.append("\t\"{0}, what's the weather like in Leander\"\n".format(name.capitalize()))
    #    message.append("\t\"{0}, how's the weather in Palm Beach, FL?\"\n".format(name.capitalize()))
    #    message.append("\t\"{0}, what's the weather in 23456?\"\n".format(name.capitalize()))
    #    message.append("You may ask me to google something:\n")
    #    message.append("\t\"{0}, google 'Why is Jimmy Buffett so awesome' please\"\n".format(name.capitalize()))
    #    message.append("\t\"{0}, can you please google good restaurants near IBM\"\n".format(name.capitalize()))
    #    message.append("You may ask me for the latest XKCD comic:\n")
    #    message.append("\t\"{0}, can you show me the latest xkcd please?\"\n".format(name.capitalize()))
    #    message.append("You may *not* talk disrespectfully to me...")
    #    message.append('\n```\n')
    #    self.assertEquals(result, ''.join(message))

    def tearDown(self):
        check_output(['cp', '/tmp/sooper', 'config/travis.cfg'])


def suite():
    """ Gather all news tests together in a suite
    """
    return unittest.TestLoader().loadTestsFromTestCase(NewsTest)

def main():
    unittest.main()

if __name__ == '__main__':
    main()
