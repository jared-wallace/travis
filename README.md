# TravisMcGee
General purpose bot for IBM Workspace
Version 0.7.1

### Purpose
This is mainly an excuse to play around with the Workspace API

### Setup and usage
# Installing:
---
  * Use `pip install -r requirements.txt` to install required
dependencies. 
  * Travis uses Django as it's web framework, so you will
either need to follow Django's instructions for configuring your webserver
of choice, or, set Travis to run in "debug" mode, which uses the built-in
development server of Django.
  * In any case, you will need to ensure the proper port is open in your
    firewall (port 1337 for the built-in dev server)
  * If you want Travis to listen to your space, you'll need to first register
  a Workspace application, and after starting Travis, add the webhook callback.
  Travis has to be running in order to verify and enable the callback. The
  callback url you register must resolve to the server you are running Travis
  on. If you hit any snags, see the logfile for help figuring out why.

# Usage
---
  * Travis is configured by editing a config file. You'll find an example
  config in the `examples` directory with sane defaults and instructions on
  filling in the missing pieces.
  * Commands to Travis must mention him by name (unless using shortcuts). See
    the examples section for more information.
  * If you make a negative comment about travis (that mentions him by name), he
    will respond appropriately.
  * You can optionally configure Travis to announce news headlines every so
    often (details on what news, how much, and how often are configured in the
    config file).
  * You can optionally set Travis to watch various rss feeds for news that matches
    a list of keywords you supply in the config.
  * You can optionally set Travis to watch various rss feeds for news that matches
    keywords that arise in normal conversation. The required number of
    mentions before Travis will search is configurable. The number specified is
    for the same interval as the news update, and Travis will search the
    previous 24 hours for any relevant news items.
  * You can ask for a funny quote, and Travis will provide a humerous quote.
  * You can also optionally configure Travis to listen to particular
    GitHub repositories, and announce new pushes or issue events (details are in
    the example config). For example, you can Travis announce new bugs for one
    repository, and all new issues for a different repository.
  * Travis will spell check your messages and offer suggestions for mispelled
    words. You can add new words to his dictionary with "!add word XXX" where
    XXX is the word to add, or remove them with "!remove word XXX".

# Command Examples
---
To get a full list of commands, type "!help"


Writing `"Travis, show me the latest xkcd"` in the channel will provide you the
link to the latest XKCD comic.


You can ask Travis what the weather is like, or get a forecast, using a zipcode or a city (Travis will do best guess on state if you pick city.

ex. `"Travis, what's the weather like in Leander?"` or `"Travis, how's the weather in 90210?"`


You can also specify a state, if you wish to help Travis select the correct location.

ex. `"How's the weather in San Antonio, FL, Travis?"`


You can ask Travis to google things for you, and he will return the quantity of results specified in the config.

ex. `"Travis, can you google 'jeff dean' please?"` or `"Travis, google awesome cat videos"`

You can also use shortcuts if you don't feel like talking, by prepending an
exclamation point.

ex. !weather

ex. !google Travis McGee

ex. !quote

You can ask Travis what RETAIN queue handles certain issues. You can also add
new RETAIN queues to the database, remove RETAIN queues from the database and
add or remove the keywords associated with each RETAIN queue.

ex. !what queue handles sametime?

ex. !what queues handle smart cloud notes?

ex. !add new queue 'ESTWK2,328' with keywords 'sametime extended work'

ex. !remove queue 'ESTWK2,328'

ex. !add keywords 'incoming' to queue 'STWWIN,328'

ex. !remove keywords 'awesome' from queue 'ESTWK2,328'

You can see the last time a Space member posted with the "!seen XXX" command, where XXX is a member.

### Planned functionality
See the "plans.txt" file


### Contributing

**Master** branch - production ready

**Development** branch - latest delivered changes
* nightly builds come from here
* when code reaches stability, merged into master and release tagged

Feature branches
* branch off from **development**
* merge into **development**
* naming convention - anything except **master**, **development**, release-* or hotfix-*
* used to develop new features for an upcoming release
* exists as long as a feature is in development, then either merged into **development** or discarded
* usually won't be present in the master repository(IBM GH)
* creating:
  * git checkout -b myfeature development
* merging:
  * git checkout development
  * git merge --no-ff myfeature
  * git branch -d myfeature
  * git push origin development

Release branches
* branch off from development
* merge into **development** and **master**
* naming convention - release-*
* supports preparation of a new production release
* allows minor bug fixes and preparing meta-data for a release
* should be branched off **development** when it's ready (or almost ready) for release
* the branching marks the time when an upcoming release gets assigned a version number
* creating:
  * git checkout -b release-1.2 development
  * ./bump-version.sh 1.2
  * git commit -a -m "Bumped version number to 1.2"
* merging:
  * git checkout master
  * git merge --no-ff release-1.2
  * git tag -a 1.2
  * git push origin 1.2
  * git checkout development
  * git merge --no-ff release-1.2 (probably gonna have a merge conflict, so just fix it)
  * git branch -d release-1.2


Hotfix branches
* branch off from master
* merge into **development** and **master**
* naming convention - hotfix-*
* similar in purpose to release branches, but unplanned
* for fixing critical bugs
* creating:
  * git checkout -b hotfix-1.2.1 master
  * ./bump-version.sh 1.2.1
  * git commit -a -m "Bumped version number to 1.2.1"
* fix the bug with one or more commits
  * git commit -m "Fixed security hole CV-111111"
* merging:
  * git checkout master
  * git merge --no-ff hotfix-1.2.1
  * git tag -a 1.2.1
  * git checkout development
  * git merge --no-ff hotfix-1.2.1
  * Exception to merging into development is when a release branch exists. In that case, merge into the release.
  * git branch -d hotfix-1.2.1

Creating a cert and key with no passphrase for local testing:
* openssl genrsa -out server.key 1024
* openssl req -new -key server.key -out server.csr
* openssl x509 -req -days 366 -in server.csr -signkey server.key -out server.crt
* openssl x509 -in server.crt -out cert.pem -outform PEM
* The cert is cert.pem, the key is server.key

How to write logging messages
* Levels:
  * CRITICAL - any error that forces shutdown to avoid data loss or prevent further data loss.
  * ERROR - any error fatal to an operation, but not the app. Example, can't open a file.
  * WARNING - anything that could cause odd but recoverable behavior
  * INFO - gernerally useful info, like service start/stop, configuration values, etc
  * DEBUG - When you need to trace a function to find a problematic statement
