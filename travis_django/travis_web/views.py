from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
import logging
import hmac
import hashlib
import socket
import sys
import os
import struct

log = logging.getLogger(__name__)

# This will be replaced by travis during initialization
server_address = '/home/admin/src/travis/travis.sock'
log.debug('Using travis socket at {0}'.format(server_address))

# This is required, or else Django rejects IBM's call
@csrf_exempt
def hook(request):
    # This will get replaced by travis during initialization
    webhook_key = '45edfgr7686h64fg5'
    # Really, we should only be getting POST
    if request.method == 'POST':
        log.debug('Received a POST call')
        # We get json from IBM, so we have to explicitly load it to get a python object
        data = json.loads(request.body)
        # Log the body
        log.debug('Json loaded data is : {0}'.format(data))
        # Log the headers and other extraneous stuff
        log.debug('Meta data is: {0}'.format(request.META))
        # We only accept a post request that has a 'type' key
        if 'type' not in data:
            return HttpResponse("Go away")
        # We handle auth requests differently
        if data['type'] == 'verification':
            challenge = data['challenge']
            log.debug('challenge is: {0} of type: {1}'.format(challenge, type(challenge)))
            # need to re-encode the body as json before hashing
            body = json.dumps({"response":challenge})
            # This specific format is required by IBM
            hash_val = hmac.new(webhook_key, body, hashlib.sha256).hexdigest()
            # I love Django's abstraction
            response = HttpResponse(body)
            # This is also required by IBM
            response['X-OUTBOUND-TOKEN'] = hash_val
            response['Content-Type']='application/json'
            return response
        else:
            # This means we got something from the space - nessage, annotation, etc
            # All we do is turn around and spit it back into the UDS
            # socket Travis is listening on.
            write_content(request)
            # Technically, an empty reponse with a 200 OK header is sufficient, but...
            return HttpResponse("Thanks IBM")
    else:
        return HttpResponse("Go away")

def write_content(request):
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    # Connect the socket to the port where the server is listening
    try:
        sock.connect(server_address)
    except socket.error, msg:
        log.debug('Could not connect to {0} because {1}'.format(server_address, msg))
    try:
        log.debug('Trying to send {0}'.format(request.body))
        msg = struct.pack('>I', len(request.body)) + request.body
        sock.sendall(msg)
    except Exception, e:
        log.debug('Failed to send to UDS because {0}'.format(e))
    finally:
        sock.close()
